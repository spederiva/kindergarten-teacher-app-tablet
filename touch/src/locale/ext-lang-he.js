Ext.Date.dayNames = [
	"ראשון",
	"שני",
	"שלישי",
	"רביעי",
	"חמישי",
	"שישי",
	"שבת"
];

Ext.Date.shortDayNames = [
    "א",
    "ב",
    "ג",
    "ד",
    "ה",
    "ו",
    "שבת"
];


Ext.Date.monthNames = [
	"ינואר",
	"פברואר",
	"מרץ",
	"אפריל",
	"מאי",
	"יוני",
	"יולי",
	"אוגוסט",
	"ספטמבר",
	"אוקטובר",
	"נובמבר",
	"דצמבר"
];

Ext.Date.monthNumbers = {
	Jan : 0,
	Feb : 1,
	Mar : 2,
	Apr : 3,
	May : 4,
	Jun : 5,
	Jul : 6,
	Aug : 7,
	Sep : 8,
	Oct : 9,
	Nov : 10,
	Dec : 11
};

Ext.Date.getShortMonthName = function(month) {
	return Ext.Date.monthNames[month].substring(0, 3);
};

Ext.Date.getMonthNumber = function(name) {
	return Ext.Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
};

Ext.Date.getShortDayName = function(day) {
	return Ext.Date.shortDayNames[day];
};

if(Ext.Date.parseCodes) {
	Ext.Date.parseCodes.S.s = '(?:st|nd|rd|th)';
}

if(Ext.picker) {
	if (Ext.picker.Picker) {
		Ext.override(Ext.picker.Picker, {
			doneText: 'אישור'
		});
	}

	if (Ext.picker.Date) {
		Ext.override(Ext.picker.Date, {
			'dayText': 'יום',
			'monthText': 'חודש',
			'yearText': 'שנה',
			'slotOrder': ['month', 'day', 'year']
		});
	}
}

//if (Ext.IndexBar) {
//	Ext.override(Ext.IndexBar, {
//		'letters': ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
//	});
//}

if(Ext.MessageBox){
	Ext.MessageBox.OK.text = 'אישור';
	Ext.MessageBox.CANCEL.text = 'ביטול';
	Ext.MessageBox.YES.text = 'כן';
	Ext.MessageBox.NO.text = 'לא';

	Ext.MessageBox.buttonText = {
		ok     : "אישור",
		cancel : "ביטול",
		yes    : "כן",
		no     : "לא"
	};
}

if(Ext.util && Ext.util.Format){
	Ext.util.Format.defaultDateFormat = 'm/d/Y';

//	Ext.util.Format.date = function(v, format){
//		if(!v) return "";
//		if(!(v instanceof Date)) v = new Date(Date.parse(v));
//		return v.dateFormat(format || "m/d/Y");
//	};
}