Ext.define('SA.data.writer.JsonForReportingInfo', {
	extend: 'Ext.data.writer.Json',
	alias: 'writer.jsonforreportinginfo',

	writeRecords: function (request, data) {
		var
			req = this.callParent(arguments),
			jsonData = req.getJsonData();

		jsonData.GroupInstanceId = request.getParams().groupInstanceId;

		this.addAndFormatObjects(jsonData);

		req.setJsonData(SA.utils.JsonForWebApi.encode(jsonData));

		return req;
	},

	addAndFormatObjects: function (data) {
		var i, len = data.BaseReportInfos.length;

		for (i = 0; i < len; i++) {
			rI = data.BaseReportInfos[i];

			if (!Ext.isEmpty(rI.ChildId)) {
				rI.ChildDto = {Id: rI.ChildId};
			}
			if (!Ext.isEmpty(rI.WorkerId)) {
				rI.WorkerDto = {Id: rI.WorkerId};
			}
			if (!Ext.isEmpty(rI.GroupInstanceId)) {
				rI.GroupInstanceDto = {Id: rI.GroupInstanceId}
			}

			//Returns the ReportingDate to the original spandate
			//and casting to YY-MM-DD format in order to send the the server
			if (Ext.isNumeric(rI.ReportingDate)) {
				rI.ReportingDate = Ext.Date.format(new Date(rI.ReportingDate * 1000), 'Y-m-d');
			}
		}
	}
});