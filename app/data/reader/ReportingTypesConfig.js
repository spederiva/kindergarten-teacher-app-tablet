Ext.define('SA.data.reader.ReportingTypesConfig', {
    extend: 'Ext.data.reader.Json',
    alias: 'reader.reportingtypesconfig',

    config:{
        model: null
    },

    readRecords: function (data) {
        var me = this;

        /**
         * @property {Object} rawData
         * The raw data object that was last passed to readRecords. Stored for further processing if needed
         */
        me.rawData = data;

        data = me.getData(data);

        if (data.metaData) {
            me.onMetaChange(data.metaData);
        }

        // <debug>
        if (!me.getModel()) {
            Ext.Logger.warn('In order to read record data, a Reader needs to have a Model defined on it.');
        }
        // </debug>

        // If we pass an array as the data, we don't use getRoot on the data.
        // Instead the root equals to the data.
        var isArray = Ext.isArray(data),
            root = isArray ? data : me.getRoot(data),
            success = true,
            recordCount = 0,
            total, value, records, message;

        if (isArray && Ext.isEmpty(data.length)) {
            return me.nullResultSet;
        }

        // buildExtractors should have put getTotal, getSuccess, or getMessage methods on the instance.
        // So we can check them directly
        if (me.getTotal) {
            value = parseInt(me.getTotal(data), 10);
            if (!isNaN(value)) {
                total = value;
            }
        }

        if (me.getSuccess) {
            value = me.getSuccess(data);
            if (value === false || value === 'false') {
                success = false;
            }
        }

        if (me.getMessage) {
            message = me.getMessage(data);
        }

        if (root) {
            records = me.extractData(root);
            recordCount = records.length;
        } else {
            recordCount = 0;
            records = [];
        }

        Ext.Array.each(data, function(item, idx){
            records[idx].data.ReportingTypeId = item;
        })

        return new Ext.data.ResultSet({
            total: total,
            count: recordCount,
            records: records,
            success: success,
            message: message
        });
    }
});