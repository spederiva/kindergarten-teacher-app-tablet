Ext.define('SA.store.ReportingInfo', {
	extend: 'Ext.data.Store',

	config: {
		model: 'SA.model.ReportingInfo',

		proxy: {
			type: 'reportinginfo'
		},

		listeners: {
			addrecords: function (store, records, eOpts) {
				var i = 0, len = records.length, reportingType, $type;

				if (!this.reportingTypesStore) {
					this.reportingTypesStore = Ext.getStore('ReportingTypes');
				}

				for (; i < len; i++) {
					reportingType = records[i].get('ReportingType');
					$type = this.reportingTypesStore.getById(reportingType).get('$type');

					records[i].set('$type', $type);
				}
			}
		}
	}
});