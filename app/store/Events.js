Ext.define('SA.store.Events', {
	extend: 'Ext.data.Store',

	config: {
		model: 'SA.model.Event'
	},

	constructor: function (config) {
		this.callParent(arguments);
	}

});