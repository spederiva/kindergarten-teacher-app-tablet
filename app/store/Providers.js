Ext.define('SA.store.Providers', {
	extend: 'Ext.data.Store',

	config: {
		model: 'SA.model.Provider',

		grouper: {
			groupFn: function (record) {
				var name = record.get('PrivateName');

				return name ? name.substr(0, 1) : null;
			},
			sortProperty: 'PrivateName'
		}
	}
});