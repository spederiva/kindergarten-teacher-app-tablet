Ext.define('SA.store.Children', {
    extend: 'Ext.data.Store',

    config: {
        model: 'SA.model.Child'
    }
});