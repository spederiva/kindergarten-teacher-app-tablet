/**
 * Created by ranwahle on 5/4/15.
 */
Ext.define('SA.store.Files', {
    extend: 'Ext.data.Store',

    config: {
        model: 'SA.model.File'
    }
});