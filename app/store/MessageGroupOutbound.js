Ext.define('SA.store.MessageGroupOutbound', {
	extend: 'Ext.data.Store',

	config: {
		model: 'SA.model.MessageGroup',

		proxy: {
			type: 'messageGroupOutbound'
		},
		pageSize: 25
	}
});