Ext.define('SA.store.Reports', {
    extend: 'Ext.data.Store',

    config: {
        model: 'SA.model.Report'
    }
});