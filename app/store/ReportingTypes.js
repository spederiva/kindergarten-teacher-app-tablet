(function () {
    var
        healthValues = [
            //{id: 0, value: 'UnKown', text: 'reportingTypes.unknown'},
            {id: 1, value: 'Well', text: 'reportingTypes.healthValues.well'},
            {id: 2, value: 'Warning', text: 'reportingTypes.healthValues.warning'}
        ],
        sleepingValues = [
            //{id: 0, value: 'UnKown', text: 'reportingTypes.unknown'},
            {id: 1, value: 'Well', text: 'reportingTypes.sleepingValues.well'},
            {id: 2, value: 'Warning', text: 'reportingTypes.sleepingValues.warning'}
        ],
        eatingValues = [
            //{id: 0, value: 'UnKonw', text: 'reportingTypes.unknown'},
            {id: 1, value: 'Well', text: 'reportingTypes.eatingValues.well'},
            {id: 2, value: 'EatLittle', text: 'reportingTypes.eatingValues.eatLittle'},
            {id: 3, value: 'EatMainMeal', text: 'reportingTypes.eatingValues.eatMainMeal'},
            {id: 4, value: 'EatSubMeal', text: 'reportingTypes.eatingValues.eatSubMeal'},
            {id: 5, value: 'NoEating', text: 'reportingTypes.eatingValues.noEating'}
        ],
        attendanceValues = [
            //{id: 0, value: 'UnKown', text: 'reportingTypes.unknown'},
            {id: 1, value: 'Attendance', text: 'reportingTypes.attendanceValues.attendance'},
            {id: 2, value: 'Absence', text: 'reportingTypes.attendanceValues.absence'}
        ],
        exitValues = [
            //{id: 0, value: 'UnKown', text: 'reportingTypes.unknown'},
            {id: 1, value: 'Well', text: 'reportingTypes.exitValues.well'},
            {id: 2, value: 'Warning', text: 'reportingTypes.exitValues.warning'}
        ],
        data = [
            {Id: 0, Type: 'Eating', Class: 'eating', Title: 'reportingTypes.titles.Eating', Property: 'ReportingEatingStatus', $type: 'SmartAfWebApi.Models.Reporting.EatingReportingInfoDTO, SmartAfWebApi', Options: eatingValues},
            {Id: 1, Type: 'Sleeping', Class: 'sleeping', Title: 'reportingTypes.titles.Sleeping', Property: 'ReportingStatus', $type: 'SmartAfWebApi.Models.Reporting.SleepingReportingInfoDTO, SmartAfWebApi', Options: sleepingValues},
            {Id: 2, Type: 'KidExit', Class: 'kidExit', Title: 'reportingTypes.titles.KidExit', Property: 'ReportingStatus', $type: 'SmartAfWebApi.Models.Reporting.KidExitReportingInfoDTO, SmartAfWebApi', Options: exitValues},
            {Id: 3, Type: 'Attendance', Class: 'attendance', Title: 'reportingTypes.titles.Attendance', Property: 'AttendanceStatus', $type: 'SmartAfWebApi.Models.Reporting.AttendanceReportingInfoDTO, SmartAfWebApi', Options: attendanceValues},
            {Id: 4, Type: 'Health', Class: 'health', Title: 'reportingTypes.titles.Health', Property: 'ReportingHealthStatus', $type: 'SmartAfWebApi.Models.Reporting.HealthReportingInfoDTO, SmartAfWebApi', Options: healthValues},
            {Id: 5, Type: 'Comment', Class: 'comment', Title: 'reportingTypes.titles.Comment', Property: 'FreeText', $type: 'SmartAfWebApi.Models.Reporting.CommentReportingInfoDTO, SmartAfWebApi', Options: null, isDefault: true},
            {Id: 6, Type: 'MissingItem', Class: 'missingItem', Title: 'reportingTypes.titles.MissingItem', Property: 'MissingItem', $type: 'SmartAfWebApi.Models.Reporting.MissingItemReportingInfoDTO, SmartAfWebApi', Options: null, isDefault: true}
        ];


    Ext.define('SA.store.ReportingTypes', {
        extend: 'Ext.data.Store',

        requires: [
            'SA.model.ReportingType',
            'SA.model.ReportingOptions'
        ],

        config: {
            model: 'SA.model.ReportingType'
        },

        load: function (options, scope) {
            var
                me = this,
                reportingTypesConfigStore = Ext.getStore('ReportingTypesConfig'),
                operation;

            Ext.applyIf(options, {
                addRecords: false,
                action: 'read',
                params: this.getParams(),
                model: this.getModel()
            });
            operation = Ext.create('Ext.data.Operation', options);

            reportingTypesConfigStore.load({
                groupInstanceId: SA.app.currentGroupInstanceId,
                callback: function (records, op, successful) {
                    console.log("ReportingTypes loaded", records);

                    if (successful) {
                        this.internalSetData(records);

                        //this is a callback that would have been passed to the 'read' function and is optional
                        Ext.callback(operation.getCallback(), operation.getScope() || me, [this.getData(), operation, successful]);
                    }
                },
                scope: this
            });

        },

        internalSetData: function (selectedReportingTypes) {
            var
                arrSelectedReportingTypes = selectedReportingTypes.map(function (item) {
                    return item.get('ReportingTypeId')
                }),
                reportingTypes = Ext.Array.filter(data, function (item) {
                    return item.isDefault === true || arrSelectedReportingTypes.indexOf(item.Id) > -1;
                });

            this.addData(reportingTypes);
        }
    });
}());