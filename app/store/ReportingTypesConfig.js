Ext.define('SA.store.ReportingTypesConfig', {
	extend: 'Ext.data.Store',

	config: {
		model: 'SA.model.ReportingTypeConfig'
	}
});