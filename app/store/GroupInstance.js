Ext.define('SA.store.GroupInstance', {
    extend: 'Ext.data.Store',
    requires: ['SA.model.File'],
    config: {
        model: 'SA.model.GroupInstance'
    }
});