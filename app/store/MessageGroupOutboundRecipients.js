Ext.define('SA.store.MessageGroupOutboundRecipients', {
	extend: 'Ext.data.Store',

	config: {
		model: 'SA.model.Child',

		proxy: {
			type: 'messageGroupOutboundRecipients'
		}
	}
});