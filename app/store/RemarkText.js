/**
 * Created by ranwahle on 9/26/15.
 */
Ext.define('SA.store.RemarkText', {
    extend: 'Ext.data.Store',
    requires: [
        'SA.model.RemarkText'
    ],
    config: {
        model: 'SA.model.RemarkText',
        data:{
            IconUrl: '../images/eventIcons/attendance/checkin.png',
            Text: 'נכח'
        }

    }
});