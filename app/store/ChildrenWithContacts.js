Ext.define('SA.store.ChildrenWithContacts', {
    extend: 'Ext.data.Store',

    config: {
        model: 'SA.model.ChildWithContacts',

	    grouper: {
		    groupFn: function(record) {
			    return record.get('PrivateName').substr(0,1);
		    },
		    sortProperty: 'PrivateName'
	    }
    }
});