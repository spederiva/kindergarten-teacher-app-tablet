Ext.define('SA.store.Parents', {
    extend: 'Ext.data.Store',

    requires: [
        'SA.model.Parent'
    ],

    config: {
        model: 'SA.model.Parent'
    }
});