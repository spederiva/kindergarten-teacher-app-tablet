Ext.define('SA.store.EventsConfirmation', {
	extend: 'Ext.data.Store',

	config: {
		model: 'SA.model.EventConfirmation'
	}
});