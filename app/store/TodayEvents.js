Ext.define('SA.store.TodayEvents', {
    extend: 'Ext.data.Store',

    config: {
        model: 'SA.model.TodayEvents'
    }
});