Ext.define('SA.store.SummaryReports', {
    extend: 'Ext.data.Store',

    config: {
        model: 'SA.model.SummaryReport'
    }
});