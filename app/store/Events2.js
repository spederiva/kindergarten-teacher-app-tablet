//TODO: This event clone store was created befause we don't know how to duplicate the store
//or how to filter the same store in order to use with two different views
Ext.define('SA.store.Events2', {
	extend: 'Ext.data.Store',

	config: {
		model: 'SA.model.Event'
	},

	constructor: function (config) {
		this.callParent(arguments);
	}

});