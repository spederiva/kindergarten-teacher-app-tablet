Ext.define('SA.store.ReportingOptions', {
	extend: 'Ext.data.Store',

	requires: [
		'SA.model.ReportingOptions'
	],

	config: {
		model: 'SA.model.ReportingOptions'
	}
});
