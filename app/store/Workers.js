Ext.define('SA.store.Workers', {
	extend: 'Ext.data.Store',

	config:{
		model: 'SA.model.Worker',

		grouper: {
			groupFn: function(record) {
				return record.get('PrivateName').substr(0,1);
			},
			sortProperty: 'PrivateName'
		}
	}
});