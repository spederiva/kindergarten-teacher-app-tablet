Ext.define('SA.controller.Children', {
	extend: 'Ext.app.Controller',

	requires: [
		'Ext.MessageBox',
		'SA.store.Children'
	],

	childrenStore: null,
	parentStore: null,
	actions: null,
	recordTapped: null,
	childDetailsPanel: null,

	config: {
		models: [
			'Child',
			'Parent',
			'TodayEvents'
		],

		stores: [
			'Children',
			'TodayEvents'
		],

		refs: {
			childActions: 'childActions',
			childActionsButtons: 'childActions button',
			actionButtons: '#actionButtons > button',

//			actionButtons_checkIn: '#actionButtons > button#btnCheckIn',
//			actionButtons_cancelAssist: '#actionButtons > button#btnCancelAssist',
//			actionButtons_checkOut: '#actionButtons > button#btnCheckOut',

			childrenMain: 'childrenMain',
			childrenCard: 'childrenCard',

			childDetailsPanel: 'childrenCard childDetailsPanel',
			childDetailsContacts: 'childrenCard childDetailsContacts',
			childDetailsInfo: 'childrenCard childDetailsInfo',
			childDetailsInfoTable: 'childrenCard childDetailsInfo #infoTable',
			childDetailsInfoImage: 'childrenCard childDetailsInfo #infoImage',
			childDetailsReport: 'childrenCard childDetailsReport',
			childDetailsContacts: 'childDetailsContacts',
			childDetailsReport: 'childDetailsReport'
		},

		control: {
			childrenMain: {
				activate: 'onActivateMain'
			},

			gridFace: {
				//itemtap: 'onItemTap'
				//itemtaphold: 'onItemTapHold'
			},

			childActionsButtons: {
				tap: 'onActionsTap'
			},

			childDetailsContacts: {
				activate: 'onActivateChildDetailsContacts'
			},

			childDetailsReport: {
				activate: 'onActivateChildDetailsReport'
			}
		}
	},

	onActivateMain: function () {
		var groupInstanceStore = Ext.getStore('GroupInstance'),
			groupInstance = groupInstanceStore.getAt(0),
			groupName = groupInstance.get('Name');

		this.loadChildren();
		this.loadTodayEvents();

		//Set the Title of the View
		this.getChildrenMain().config.title = "Report problems for: " + groupName;

		console.log('ChildrenMain Controller');
	},

	onActivateChildDetailsContacts: function () {
		this.parentStore = Ext.getStore('Parents');

		this.parentStore.load(function (records, operation, successful) {
			console.log("Parents loaded", this.parentStore.data.length, this.parentStore.data);
		}, this);
	},

	onActivateChildDetailsReport: function(){
		this.loadReports();
		this.loadSummaryReports();
	},

	loadChildren: function () {
		if (Ext.isEmpty(this.childrenStore)) {
			this.childrenStore = Ext.getStore('Children');

			this.childrenStore.load({
				groupInstanceId: SA.app.currentGroupInstanceId,
				callback: function (records, operation, successful) {
					console.log("Children loaded", this.childrenStore.data.length, this.childrenStore);
				},
				scope: this
			});
		}
	},

	loadTodayEvents: function () {
		var todayEvents = Ext.getStore('TodayEvents');

		todayEvents.load(function () {
			console.log("TodayEvents - TodayEvents", todayEvents.data.length, todayEvents.data);
		}, this);
	},

	loadReports: function(){
		return;
		Ext.getStore('Reports').load(function (records, operation, successful) {
			console.log("Reports loaded", records.length, records);
		}, this);
	},

	loadSummaryReports: function(){
		Ext.getStore('SummaryReports').load(function (records, operation, successful) {
			console.log("SummaryReports loaded", records.length, records);
		}, this);
	},

	onItemTap: function (self, index, target, record, e, opts) {
		this.recordTapped = record;

		this.showActions();

		this.selectChild(record);

//		this.changeStateCheckButtons(isAssist);
	},

	showActions: function () {
		this.getChildActions().show();
	},

	selectChild: function (record) {
		var isSelected = record.get('IsSelected');

		this.updateChildIsSelected(record, !isSelected);
	},

//	onItemTapHold: function (self, index, target, record, e, opts) {
//		var isAssist = record.get('isAssist');
//
//		if (!isAssist) {
//			this.updateChildAssist(record, true);
//		}
//
//		e.stopEvent();
//		e.stopPropagation();
//		target.onBefore('tap', function (e) {
//			e.stopEvent();
//		}, this, {single: true});
//	},

	onActionsTap: function (self, e, opts) {
		var that = this;

		switch (self.id) {
			case 'btnDetails':
				this.openDetails();
				break;
			case 'btnCheckIn':
				this.updateChildAssist(this.actions.getData(), true);
				break;
			case 'btnCancelAssist':
				Ext.Msg.confirm('Cancel Check-In', 'Are you sure you want to cancel the \'Check-In\'', function (buttonId, value, opt) {
					if (buttonId === 'yes') {
						that.updateChildAssist(that.actions.getData(), false);
					}
				});

				break;
			case 'btnCheckOut':
				this.updateChildAssist(this.actions.getData(), false);
				break;
			case 'btnClose':
				break;
			default:
				Ext.Msg.alert('Not Implemented', 'This feature is not implemented yet', Ext.emptyFn);
				break;
		}

		//this.actions.hide();
	},

	updateChildIsSelected: function (record, isSelected) {
		record.set('IsSelected', isSelected);

//		record.save({
//			scope: this,
//			success: function (record) {
//				console.log('this.proxySuccess', arguments);
//
//				record.set('checkInUpdate', new Date());
//			},
//			failure: function () {
//				console.log('this.proxyFailure', arguments);
//			},
//			callback: function () {
//				console.log('this.proxyCallback', arguments);
//			}
//		});
	},

	changeStateCheckButtons: function (isCheckedIn) {
		if (!isCheckedIn) {
			this.getActionButtons_checkIn().show();
			this.getActionButtons_cancelAssist().hide();
			this.getActionButtons_checkOut().hide();
		} else {
			this.getActionButtons_checkIn().hide();
			this.getActionButtons_cancelAssist().show();
			this.getActionButtons_checkOut().show();
		}
	},

	openDetails: function (list, idx, el, record) {
		if (!this.childDetailsPanel) {
			this.childDetailsPanel = Ext.widget('childDetailsPanel');
		}

		this.childDetailsPanel.config.title = this.recordTapped.getFullName();

		var childrenCard = this.getChildrenCard();
		childrenCard.setRecord(this.recordTapped);
		childrenCard.push(this.childDetailsPanel);

		//Push record into view
		this.getChildDetailsInfo().setRecord(this.recordTapped);
		this.getChildDetailsContacts().setRecord(this.recordTapped);
		this.getChildDetailsReport().setRecord(this.recordTapped);
		this.getChildDetailsInfoTable().setRecord(this.recordTapped);
		this.getChildDetailsInfoImage().setRecord(this.recordTapped);
	}

})
;