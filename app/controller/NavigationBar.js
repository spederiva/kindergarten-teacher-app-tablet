Ext.define('SA.controller.NavigationBar',{
    extend: 'Ext.app.Controller',

    config:{
        refs: {
            main: 'main',
            messagesCard: 'messagesCard'
        },

        control: {
            main:{
                initialize: 'onMainInit'
            },

            messagesCard: {
                push: 'onMainPush',
                pop: 'onMainPop'
            }
        }
    },

    onMainInit: function(){
        this.tabBar = this.getMain().getTabBar();
    },

    hideMainTabBar:function(){
        this.tabBar.hide();
    },

    showMainTabBar:function(){
        this.tabBar.show();
    },

    onMainPush: function(view, item) {
        if(view.getNavigationBar && view.getNavigationBar()) {
            this.hideMainTabBar();
            view.getNavigationBar().show();
        }
    },

    onMainPop: function(view, item) {
        if(view.getNavigationBar && view.getNavigationBar()) {
            this.showMainTabBar();
            view.getNavigationBar().hide();
        }
    }
});