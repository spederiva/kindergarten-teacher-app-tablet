﻿Ext.define('SA.controller.Login', {
    extend: 'Ext.app.Controller',

    requires: [
        'SA.model.Login',
        'SA.model.GroupInstance',
        'Ext.MessageBox'
    ],

    config: {
        refs: {
            mainContainer: 'mainContainer',
            loginButton: '#login-button',
            forgotPasswordButton: '#forgotPassword-button',
            loginForm: '#loginForm',
            login: 'login'
        },

        control: {
            login:{
                show: 'onShow'
            },
            loginButton: {
                tap: 'submitLoginForm'
            },
            forgotPasswordButton:
            {
                tap: 'forgotPasswordTap'
            }
        }
    },

    onShow:function(me){
        me.setMasked(false);
    },

    forgotPasswordTap: function()
    {
        var  loginForm = this.getLoginForm().getFields(),
            login = loginForm.email.getValue();
        this.forgotPassword(login);
    },

    forgotPassword: function(user) {
        var loginUser = Ext.create('SA.model.ForgotPassword', {
            Email: user

        });

        loginUser.save({
            success: function () {
                var MB = Ext.MessageBox;
                Ext.apply(MB, {OK: {text: Ux.locale.Manager.get('buttons.done')}});
                Ext.Msg.alert('', Ux.locale.Manager.get('login.passwordSent'), Ext.emptyFn);
            }
        });
    },
    launch: function () {

        var
            nologinUser = Helper.getUrlParams('nologin'),
            password = Helper.getUrlParams('password');

        if (!Ext.isEmpty(nologinUser)) {
            var loginForm = this.getLoginForm().getFields();

            loginForm.email.setValue(nologinUser);
            loginForm.password.setValue(password || '1234');
            this.loggedInAutomaticly = true;
            this.submitLoginForm();
        }
    },

    submitLoginForm: function (self, e) {
        var
            that = this,
            loginForm = this.getLoginForm().getFields(),
            loginUser = Ext.create('SA.model.Login', {
                email: loginForm.email.getValue(),
                password: loginForm.password.getValue()
            });

        this.getLogin().setMasked(true);

        loginUser.save({
            success: function (record, operation) {
                var response = Ext.decode(operation.getResponse().responseText);

                if (!that.setInit(response)) {
                    this.showErrorMessage();

                    return;
                }

                Ext.Viewport.add({ xtype: 'main' }).show();
            },
            failure: function (record, operation) {
                console.log('this.proxyFailure', arguments);
                if (!that.loggedInAutomaticly) {
                    this.showErrorMessage();
                }
                else{
                    that.getLogin().setMasked(true);
                    that.loggedInAutomaticly = false;
                }
            }
        }, this);
    },

    setInit: function (response) {
        console.log(response);
        if (!(response && +response.Id > 0 && response.GroupInstances && response.GroupInstances.length > 0)) {
            return false;
        }

        console.log('Login Success', response);

        SA.app.currentKindergartenId = response.KindergartenId;
        //Set CurrentGroupInstanceId
        SA.app.currentGroupInstanceId = response.GroupInstances[0].Id;//[0].id;

        //Set Current User Model
        SA.app.currentWorker = this.getWorker(response);

        //Set the store to the new parameters
        Ext.getStore('GroupInstance').setData(response.GroupInstances);

        return true;
    },

    getWorker: function (response) {
        var worker = Ext.create('SA.model.Worker');
        worker.setData(response);

        return worker;
    },

    showErrorMessage: function () {
        this.getLogin().setMasked(false);

        Ext.Msg.alert('User/Pass Incorrect', 'User or Password Incorrect', Ext.emptyFn);
    }

});