Ext.define('SA.controller.Reports', {
    extend: 'Ext.app.Controller',

    requires: [
        'Ext.MessageBox',
        'SA.model.Report',
        'SA.model.ReportingInfo',
        'SA.model.ReportingType',
        'SA.model.ReportingTypeConfig',
        'SA.model.ReportingOptions'
    ],

    config: {
        refs: {
            reportsCard: 'reportsCard',
            reportsMain: 'reportsMain',
            datePicker: 'reportsMain datepickerfield',
            reportsList: 'reportsList',
            childButtons: 'reportsListItem button',
            reportsModalReport: {
                xtype: 'reportsModalReport',
                selector: 'reportsModalReport',
                autoCreate: true
            },
            reportsActionsSave: 'reportsCard button[action=save]',
            reportsChildMain: {
                xtype: 'reportsChildMain',
                selector: 'reportsChildMain'
            },
            reportsListHeader: 'reportsListHeader'
        },

        control: {
            reportsCard: {
                activate: 'initView',
                deactivate: 'removeStoreFromList'
            },

            datePicker: {
                change: 'onDateChanged'
            },

            'reportsListItem > button': {
                tap: 'changeReporting',
                taphold: 'openReportingDialog'
            },

            'reportsListItem > image': {
                tap: 'presentChildDetails',
                taphold: 'presentChildDetails'
            },

            'reportsModalReport button#saveButton': {
                tap: 'updateReports'
            },

            'reportsModalReport button#closeButton': {
                tap: 'closeModal'
            },

            reportsActionsSave: {
                tap: 'submitReports'
            },

            'reportsListHeader button': {
                tap: 'openReportingDialogGroup'
            }
        }
    },

    initView: function () {
        var groupInstance = this.getGroupInstance();

        this.getReportsMain().setData(groupInstance);

        this.getStores();

        this.loadReportingTypes();

        this.loadChildren(new Date());
    },

    removeStoreFromList: function () {
        this.removeReportsReportingInfoStore();

        //Clear All Data in case the store was already loaded
        this.reportsStore.removeAll();

        this.childrenStore.clearFilter(true);

        this.getReportsList().setStore(null);

        this.reportingTypesStore = null;
        this.childrenStore = null;
        this.reportsStore = null;
    },

    removeReportsReportingInfoStore: function () {
        this.reportsStore.each(function (item) {
            var reportingInfoStore = item.ReportingInfo && item.ReportingInfo();

            reportingInfoStore && reportingInfoStore.removeAll();
        });
    },

    getGroupInstance: function () {
        var
            groupInstanceStore = Ext.getStore('GroupInstance'),
            groupInstance = groupInstanceStore.getById(SA.app.currentGroupInstanceId);

        return groupInstance;
    },

    getStores: function () {
        this.reportingTypesStore = Ext.getStore('ReportingTypes');
        this.childrenStore = Ext.getStore('Children');
        this.reportsStore = Ext.getStore('Reports');
    },

    loadReportingTypes: function () {
        var that = this;

        this.reportingTypesStore.load({
            groupInstanceId: SA.app.currentGroupInstanceId,
            callback: function (records, operation, successful) {
                that.setReportingTypesToListHeader();

                console.log("ReportingTypes loaded", records);
            },
            scope: this
        });
    },

    loadChildren: function (date) {
        var that = this;

        this.childrenStore.load({
            groupInstanceId: SA.app.currentGroupInstanceId,
            callback: function (records, operation, successful) {
                console.log("Children Reports loaded", that.childrenStore.data.length, that.childrenStore);

                that.loadReports(date);
            },
            scope: this
        });
    },

    loadReports: function (date) {
        var
            that = this,
            day = date.getDate(),
            month = date.getMonth() + 1,
            year = date.getFullYear();

        //store current date
        this.currentDate = date;

        this.reportsStore.load({
            data: {
                groupInstanceId: SA.app.currentGroupInstanceId,
                day: day,
                month: month,
                year: year
            },
            callback: function (records, operation, successful) {
                console.log("Reports loaded", records.length, that.reportsStore);

                that.setReportsListStore();
            },
            scope: this
        });
    },

    setReportsListStore: function () {
        var reportsStore = this.addChildrenToReportsStore();

        //Set the store for the view
        this.getReportsList().setStore(reportsStore);
    },

    addChildrenToReportsStore: function () {
        var
            reportsStore = this.reportsStore,
            reportingTypesStore = this.reportingTypesStore;

        this.childrenStore.getData().each(function (children, i) {
            var
                reportingTypes,
                reportingType,
                childId = children.getId(),
                report = reportsStore.getById(childId),
                isNew = Ext.isEmpty(report);

            if (isNew) {
                report = Ext.create('SA.model.Report');
            }

            //Add ReportingTypes Configuration (in order to show relevant columns)
            reportingTypes = report.ReportingType();
            reportingTypes.setData(reportingTypesStore.getData().all);

            //Set Child to the row
            report.setChild(children);

            //Add to store
            if (isNew) {
                reportsStore.add(report);
            }
        });

        console.log("ReportsStore object created", reportsStore.data.length, reportsStore);

        return reportsStore;
    },

    onDateChanged: function (obj, newDate, oldDate, eOpts) {
        this.removeStoreFromList();

        this.getStores();
        this.loadChildren(newDate);

        this.disableSaveButton();
    },

    changeReporting: function (me) {
        if (this.isToday()) {
            var record, reportingTypes, selectedReportingTypeId;

            if (!this.getReportsModalReport().isPainted() && this.currentDate < Ext.Date.now()) {
                record = me.getParent().getRecord();
                reportingTypes = this.getReportingTypeByAction(me.getAction());
                selectedReportingTypeId = this.getNextReportingTypeId(record, reportingTypes);

                if (selectedReportingTypeId) {
                    this.updateRecordReports(record, reportingTypes, selectedReportingTypeId);
                } else {
                    this.openReportingDialog(me);
                }
            }
        }
    },

    presentChildDetails: function (me) {
        var record = me.getParent().getRecord();

        this.childrenStore.clearFilter(true);
        this.childrenStore.filter([]);
        this.childrenStore.filter([
            {
                filterFn: function (item) {
                    return item.data.Id === record.data.Child.Id;
                }
            }
        ]);

        var view = Ext.Viewport.add({xtype: 'reportsChildMain'});

        view.show();
    },

    getNextReportingTypeId: function (record, reportingTypes) {
        var that = this,
            options = reportingTypes.get('Options'),
            reportingTypeId,
            reportingTypeProperty,
            reportingInfo,
            reportingStatus,
            reportingTypeIdx;

        if (!options) {
            return null;
        }

        reportingTypeId = reportingTypes.getId();
        reportingTypeProperty = reportingTypes.get('Property');
        reportingInfo = this.getReportingInfo(record, reportingTypeId);
        reportingStatus = reportingInfo && reportingInfo.get(reportingTypeProperty);
        reportingTypeIdx;

        if (reportingStatus) {
            Ext.Array.each(options, function (item, i) {
                if (item.id === reportingStatus) {
                    reportingTypeIdx = i;

                    return false;
                }
            });

            if (reportingTypeIdx + 1 < options.length) {
                return options[reportingTypeIdx + 1].id;
            }
        }

        return options[0].id;
    },

    setReportingTypesToListHeader: function () {
        this.getReportsListHeader().setReportingTypes(this.reportingTypesStore);
    },

    openReportingDialog: function (me) {
        if (this.isToday()) {
            var record;

            if (this.currentDate < Ext.Date.now()) {
                record = me.getParent().getRecord();

                this.openDialog(record, me.getAction());
            }
        }
    },

    openReportingDialogGroup: function (me) {
        if (this.isToday()) {
            if (this.currentDate < Ext.Date.now() && !Ext.isEmpty(me.getAction())) {
                this.openDialog(null, me.getAction());
            }
        }
    },

    openDialog: function (record, action) {
        var data = {
            record: record,
            reportingTypes: this.getReportingTypeByAction(action),
            freeTextReportingTypeId: this.getReportingTypeByAction(action === 'MissingItem' ? 'MissingItem' : 'Comment').getId()
        }, self = this;

        this.reportsModalReport = this.getDialogAndAddToViewPort();

        var remarkTexts, remarkTextStore = Ext.getStore('RemarkText');
        remarkTextStore.load({
            callback: function(records)
            {
                remarkTexts = records;
                self.reportsModalReport.updateRemarkText(records);
            }
        });



        this.reportsModalReport.setData(data);

        this.reportsModalReport.show();
    },

    getReportingTypeByAction: function (action) {
        var reportingType = this.reportingTypesStore.findRecord("Type", action);

        return reportingType;
    },

    getReportingTypeById: function (reportingTypeById) {
        var reportingType = this.reportingTypesStore.getById(reportingTypeById);

        return reportingType;
    },

    getDialogAndAddToViewPort: function () {
        var dialog = this.getReportsModalReport();

        if (dialog && !dialog.getParent()) {
            Ext.Viewport.add(dialog);
        }

        return dialog;
    },

    closeChildDetails: function () {
        this.isOpenedModal = false;
        this.reportsChildMain.hide();
    },

    closeModal: function () {
        this.reportsModalReport.hide();
    },

    isToday: function () {
        var isToday = Ext.Date.diff(this.currentDate, new Date(), Ext.Date.DAY) === 0;

        if (!isToday) {
            Ext.Msg.alert(Ux.locale.Manager.get('reports.isNotTodayTitle'),
                Ux.locale.Manager.get('reports.isNotTodayMessage'), Ext.emptyFn);
        }

        return isToday;
    },

    updateReports: function (button, e, eOpts) {
        var data = button.getData();

        if (data.freeTextReportingTypeId) {
            if (data.record) {
                this.updateRecordReports(data.record, data.reportingTypes, data.selectedReportingTypeId, data.freeTextReportingTypeId, data.comments);
            } else {
                this.updateAllRecordsReports(data.reportingTypes, data.selectedReportingTypeId);
            }
        }

        this.closeModal();
    },

    updateAllRecordsReports: function (reportingTypes, selectedReportingTypeId) {
        var that = this;

        this.reportsStore.each(function (child) {
            that.updateRecordReports(child, reportingTypes, selectedReportingTypeId, null);
        });
    },

    updateRecordReports: function (record, reportingTypes, selectionId, freeTextReportingTypeId, freeText) {
        var
            workerId = SA.app.currentWorker.getId(),
            childId = record.ChildHasOneInstance.getId(),
            reportingTypeId = reportingTypes.getId(),
            reportingTypeProperty = reportingTypes.get('Property'),
            reportingInfo = this.getReportingInfo(record, reportingTypeId) || this.createNewReportInfo(record, reportingTypeId, childId, workerId),
            $type = reportingTypes.get('$type'),
            options = reportingTypes.get('Options');

        //Set the value according to the selection on the dialog
        if (options) {
            reportingInfo.set(reportingTypeProperty, selectionId);
        }

        //Set Comment
        if (freeText) {
            this.updateFreeText(record, childId, workerId, new Date(), freeTextReportingTypeId, freeText);
        }

        //This cause to refresh the model the model values, so the view
        record.commit();

        this.enableSaveButton();
    },

    getReportingInfo: function (record, reportingTypeId) {
        var reportingIndex = record.ReportingInfoStore.find('ReportingType', reportingTypeId);

        if (reportingIndex > -1) {
            return record.ReportingInfoStore.getAt(reportingIndex);
        }

        return null;
    },

    createNewReportInfo: function (record, reportingTypeId, childId, workerId) {
        var reportingInfo = Ext.create('SA.model.ReportingInfo');
        reportingInfo.set('ReportingType', reportingTypeId);
        reportingInfo.set('ChildId', childId);
        reportingInfo.set('WorkerId', workerId);
        reportingInfo.set('GroupInstanceId', SA.app.currentGroupInstanceId);
        reportingInfo.set('ReportingDate', new Date());

        reportingInfo.getFields().getByKey('Id').setPersist(false);

        record.ReportingInfoStore.add(reportingInfo);

        return reportingInfo;
    },

    updateFreeText: function (record, childId, workerId, reportingDate, freeTextReportingTypeId, freeText) {
        var commentReportingType, reportingTypeProperty, reportingIndex, comment,
            reportingInfoStore = record.ReportingInfoStore;

        if (!Ext.isEmpty(freeText)) {
            //commentReportingType = this.getReportingTypeByAction('Comment');
            commentReportingType = this.getReportingTypeById(freeTextReportingTypeId);
            //reportingTypeId = commentReportingType.getId();
            reportingTypeProperty = commentReportingType.get('Property');
            reportingIndex = reportingInfoStore.find('ReportingType', freeTextReportingTypeId);

            if (reportingIndex > -1) {
                comment = reportingInfoStore.getAt(reportingIndex);
            } else {
                comment = this.createNewReportInfo(record, freeTextReportingTypeId, childId, workerId, SA.app.currentGroupInstanceId, reportingDate);
                reportingInfoStore.add(comment);
            }

            comment.set(reportingTypeProperty, freeText);
        }
    },

    enableSaveButton: function () {
        this.actionSave = this.actionSave || this.getReportsActionsSave();

        if (this.actionSave.isDisabled()) {
            this.actionSave.enable();
        }
    },

    disableSaveButton: function () {
        this.actionSave = this.actionSave || this.getReportsActionsSave();

        if (!this.actionSave.isDisabled()) {
            this.actionSave.disable();
        }
    },

    submitReports: function (button, e, eOpts) {
        var
            date = new Date(),
            reportingInfo = this.getAndSetReportingInfoStore(date);

        reportingInfo.sync({
            success: function (record, operation) {
                console.log('Report Submitted Succeeded', record, operation);

                Ext.Msg.alert(Ux.locale.Manager.get('reports.title'),
                    Ux.locale.Manager.get('reports.savedSuccessfully'), Ext.emptyFn);
            },
            failure: function (record, operation) {
                console.error('Report SyncFailure', arguments);

                Ext.Msg.alert(Ux.locale.Manager.get('reports.title'),
                    Ux.locale.Manager.get('reports.savedError'), Ext.emptyFn);
            }
        });
    },

    getAndSetReportingInfoStore: function (date) {
        var
            reportsStore = Ext.getStore('Reports'),
            reportingInfo = Ext.getStore('ReportingInfo'),
            day = date.getDate(),
            month = date.getMonth() + 1,
            year = date.getFullYear();

        //Clear data if there was an previous set to the store
        reportingInfo.clearData();

        //Loop in order to fill ReportingInfo store
        reportsStore.getData().each(function (report) {
            reportingInfo.add(report.ReportingInfoStore.getData().all);
        });

        reportingInfo.getProxy().setParams({
            groupInstanceId: SA.app.currentGroupInstanceId,
            day: day,
            month: month,
            year: year
        });

        reportingInfo.each(function (record) {
            record.setDirty();
            record.phantom = true;
        });

        return reportingInfo;
    }
});