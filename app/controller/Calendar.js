Ext.define('SA.controller.Calendar', {
    extend: 'Ext.app.Controller',

    requires: [],

    config: {
        models: [
            'Event',
            'EventConfirmation'
        ],

        stores: [
            'Events'
        ],

        refs: {
            calendarCard: 'calendarCard',
            calendarMain: 'calendarMain',
            calendarView: 'calendarView',
            eventList: '#eventList',
            eventListTitle: '#eventListTitle',
            eventNavigationView: '#eventNavigationView',
            addEventButton: 'button[action=addEvent]',
            removeEventButton: 'button[action=removeEvent]',
            backEventButton: 'button[action=back]',
            submitEventButton: 'button[action=submitEvent]',
            eventForm: {
                xtype: 'eventform',
                selector: 'eventform',
                autoCreate: true
            },
            eventView: {
                xtype: 'eventview',
                selector: 'eventview',
                autoCreate: true
            }
        },

        control: {
            calendarCard: {
                activate: 'initView'
            },

            addEventButton: {
                tap: 'addEvent'
            },

            backEventButton: {
                tap: 'backEvent'
            },

            removeEventButton: {
                tap: 'removeEvent'
            },

            submitEventButton: {
                tap: 'submitEvent'
            },

            eventList: {
                initialize: 'onEventListInit',
                itemtap: 'onEventListItemTap'
            },

            eventListTitle: {
                initialize: 'onEventListTitleInit'
            },

            calendarView: {
                selectionchange: 'onCalendarViewSelectionChange',
                periodchange: 'onDateChange'
            }
        }
    },

    initView: function () {
        this.eventStore = Ext.getStore("Events");

        this.getEvents(this.currentDate || new Date());
    },

    onEventListInit: function (thisInstance, eOpts) {
        this.filterStoreByDate(thisInstance);
    },

    onEventListTitleInit: function (thisInstance, eOpts) {
        this.setEventListTitleDate(thisInstance);
    },

    filterStoreByDate: function (thisInstance, date) {
        var d = !date ? new Date() : date,
            dateFrom = new Date(d.getFullYear(), d.getMonth(), d.getDate()),
            dateTo = new Date(d.getFullYear(), d.getMonth(), d.getDate() + 1),
            store = thisInstance.getStore();

        store.clearFilter();

        store.filter([
            {
                filterFn: function (item) {
                    var eventTime = item.get('EventTime');

                    return eventTime >= dateFrom && eventTime < dateTo;
                }
            }
        ]);
    },

    setEventListTitleDate: function (thisInstance, date) {
        thisInstance.setData({
            today: !date ? new Date() : date
        });
    },

    getEvents: function (date) {
        //TODO: The Event2 store is loaded manually from 'Event' store.
        //      This is because we doesn't know how to share
        //      the same store for two different filters
        var
            that = this,
            month = date.getMonth() + 1,
            year = date.getFullYear();

        this.currentDate = date;

        this.eventStore.load({
            data: {
                groupInstanceId: SA.app.currentGroupInstanceId,
                month: month,
                year: year
            },
            callback: function (records, operation, successful) {
                console.log("Events loaded", records.length, records);

                Ext.getStore("Events2").setData(records);

                that.getCalendarView().updateViewMode();
            }
        });
    },

    onDateChange: function (calendarObj, minDate, maxDate, direction, eOpts) {
        var date = this.currentDate;

        if (this.eventStore && this.eventStore.isLoaded() && direction != 'none') {
            if (direction === 'forward') {
                date = Ext.DateExtras.add(date, Ext.Date.MONTH, 1);
            } else {
                date = Ext.DateExtras.add(date, Ext.Date.MONTH, -1);
            }

            this.getEvents(date);
        }
    },

    setUtcDate: function (date) {
        date = date || new Date();
        date.setMinutes(date.getMinutes() - date.getTimezoneOffset());

        return date;

    },
    setLocalDate: function (date) {
        date = date || new Date();
        date.setMinutes(date.getMinutes() + date.getTimezoneOffset());

        return date;

    },

    addEvent: function () {
        var eventForm = this.getEventForm();
        eventForm.setData({
            date: this.selectedDate || new Date()
        });

        this.getEventNavigationView().push(eventForm);

        this.toggleButtons('show', 'hide', 'hide');

        this.getCalendarView().setMasked(true);
    },

    backEvent: function () {
        this.getEventNavigationView().pop();

        this.toggleButtons('hide', 'show', 'hide');

        this.getCalendarView().setMasked(false);
    },

    submitEvent: function () {
        var form = this.getEventForm(),
            fields = form.getFields(),
            event = Ext.create('SA.model.Event', {
                EventTime: new Date(fields.eventTime.getValue()),
                PublishTime: new Date(),
                Subject: fields.subject.getValue(),
                Body: fields.body.getValue(),
                Place: fields.place.getValue(),
                IsInvitedParent: fields.isInvitedParent.getValue() === 1,
                GroupInstanceId: SA.app.currentGroupInstanceId
            });

        //Avoid sending the 'Id' field
        event.getFields().getByKey('id').setPersist(false);
        event.getFields().getByKey('Id').setPersist(false);


        event.save({
            success: function (record, operation) {
                Ext.getStore('Events').addData(event);
                Ext.getStore('Events2').addData(event);
                event.getFields().getByKey('Id').setPersist(true);
                this.backEvent();

                this.getCalendarView().updateViewMode();
            },
            failure: function (record, operation) {
                console.log('this.proxyFailure', arguments);

                Ext.Msg.alert(Ux.locale.Manager.get('calendar.errorSavingTitle'), Ux.locale.Manager.get('calendar.errorSavingMessage'), Ext.emptyFn);
            }
        }, this);
    },

    removeEvent: function (self) {
        Ext.Msg.confirm(Ux.locale.Manager.get('calendar.ConfirmEventDelete'),
            Ux.locale.Manager.get('calendar.confirmRemovingMessage'), function (buttonId) {
                if (buttonId === "yes") {
                    var record = self.getRecord();


                    record.getFields().getByKey('id').setPersist(false);

                    record.erase({
                        success: function (record, operation) {
                            Ext.getStore('Events').remove(record);
                            Ext.getStore('Events2').remove(record);

                            this.backEvent();

                            this.getCalendarView().updateViewMode();

                            Ext.Msg.alert(Ux.locale.Manager.get('calendar.erasingTitle'), Ux.locale.Manager.get('calendar.successRemovingMessage'), Ext.emptyFn);

                        },
                        failure: function (record, operation) {
                            console.log('this.proxyFailure', arguments);

                            Ext.Msg.alert(Ux.locale.Manager.get('calendar.errorSavingTitle'), Ux.locale.Manager.get('calendar.errorRemovingMessage'), Ext.emptyFn);
                        }
                    }, this);
                }
            }, this);


    },

    onEventListItemTap: function (list, index, target, record, e, eOpts) {
        this.getCalendarView().setMasked(true);

        this.viewEventDetails(list, index, target, record, e, eOpts);
    },

    viewEventDetails: function (list, index, target, record) {
        var eventView, d, eventConfirmationStore;

        if (!Ext.isEmpty(record.data)) {
            eventView = this.getEventView();
            d = record.data;

            eventConfirmationStore = Ext.getStore('EventsConfirmation');
            eventConfirmationStore.load({
                eventId: record.get('Id'),
                callback: function (record, operation, successful) {
                    if (successful) {
                        console.log("EventsConfirmation loaded", record, operation, successful);

                        //Add the EventConfirmation to the object to be set to the view
                        d.eventConfirmation = record[0];

                        //Set the data to be used by the view
                        eventView.setEvent(d);
                    } else {
                        console.error("EventsConfirmation error", operation);
                    }
                }
            });

            this.getEventNavigationView().push(eventView);

            this.toggleButtons('show', 'hide', 'show');

            this.setRemoveButtonRecord(record);
        } else {
            alert('Error!');
        }
    },

    toggleButtons: function (back, add, remove) {
        this.getBackEventButton()[back]();

        this.getAddEventButton()[add]();

        this.getRemoveEventButton()[remove]();

        if (remove === 'hide') {
            this.setRemoveButtonRecord(null);
        }
    },

    setRemoveButtonRecord: function (data) {
        this.getRemoveEventButton().setRecord(data);
    },

    onCalendarViewSelectionChange: function (calendarView, newDate, previousValue) {
        this.selectedDate = newDate;

        this.filterStoreByDate(this.getEventList(), newDate);
        this.setEventListTitleDate(this.getEventListTitle(), newDate);
    }
});