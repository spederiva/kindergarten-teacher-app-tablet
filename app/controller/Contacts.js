Ext.define('SA.controller.Contacts', {
	extend: 'Ext.app.Controller',

	activeStore: null,
	workers: null,
	providers: null,
	children: null,

	config: {
		models: [
			'Worker',
			'Provider',
			'ChildWithContacts'
		],

		stores: [
			'Workers',
			'Providers',
			'ChildrenWithContacts'
		],

		refs: {
			contactsCard: 'contactsCard',
			contactsMain: 'contactsMain',
			contactsWorkers: 'contactsWorkers',
			contactsProviders: 'contactsProviders',
			contactsGroups: 'contactsGroups',
			searchName: '#SearchName'
		},

		control: {
			contactsCard: {
				activate: 'initView'
			},

			contactsWorkers: {
				activate: 'getWorkers'
			},

			contactsProviders: {
				activate: 'getProviders'
			},

			searchName: {
				keyup: 'onSearchKeyUp',
				clearicontap: 'clearSearch'
			}
		}
	},

	initView: function () {
		console.info('TODO:Contacts onActivateMain events, but it wasnot activated yet');

        this.getChildrenWithContacts();
	},

	getChildrenWithContacts: function () {
		var children;

		if (SA.app.currentGroupInstanceId > 0) {
			children = Ext.getStore('ChildrenWithContacts');

			children.load({
				groupInstanceId: SA.app.currentGroupInstanceId,
				callback: function (records, operation, successful) {
					if(successful) {
						console.log("ChildrenWithContacts loaded", records.length, records);
					} else {
						console.error("ChildrenWithContacts error", operation);
					}
				},
				scope: this
			});

			this.changeActiveStore(this.children);
		}
	},

	getWorkers: function () {
		var workers;

		if (SA.app.currentGroupInstanceId > 0) {
			workers = Ext.getStore('Workers');

			workers.load({
				kindergartenId: SA.app.currentKindergartenId,
				groupInstanceId: SA.app.currentGroupInstanceId,
				callback: function (records, operation, successful) {
					if(successful) {
						console.log("Workers loaded", records.length, records);
					} else{
						console.error("Workers error", operation);
					}
				},
				scope: this
			});

			this.changeActiveStore(workers);
		}
	},

	getProviders: function () {
		var providers;

		if (SA.app.currentGroupInstanceId > 0) {
			providers = Ext.getStore('Providers');

			providers.load({
				groupInstanceId: SA.app.currentGroupInstanceId,
				callback: function (records, operation, successful) {
					if(successful) {
						console.log("Providers loaded", records.length, records);
					} else{
						console.error("Providers error", records);						
					}
				},
				scope: this
			});

			this.changeActiveStore(this.providers);
		}
	},

	changeActiveStore: function (store) {
		this.clearSearch();

		this.activeStore = store;
	},

	clearSearch: function () {
		if (this.activeStore) {
			this.activeStore.clearFilter(true);
			this.activeStore.filter([]);
		}
	},

	onSearchKeyUp: function (thisField, e, eOpts) {
		var filterString = thisField.getValue().toLowerCase();

		//Clear filter in order to search from the whole data
		//Otherwise, it will filter the filtered data
		this.activeStore.clearFilter(true);

		this.activeStore.filter([
			{
				filterFn: function (item) {
					return item.get("KeyForSearch").indexOf(filterString) > -1;
				}
			}
		]);
	}
});