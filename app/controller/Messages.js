Ext.define('SA.controller.Messages', {
    extend: 'Ext.app.Controller',

    requires: [
        'SA.model.Child',
        'SA.model.PostMessageGroupOutbound'
    ],

    newMessageTo: [],
    isAllSelected: false,

    config: {
        refs: {
            messagesCard: 'messagesCard',
            newMessageButton: 'messagesCard button[action=newMessage]',
            newMessageContainer: {
                xtype: 'newMessageContainer',
                selector: 'newMessageContainer',
                autoCreate: true
            },
            newMessageForm: 'newMessageForm',
            newMessageFormSubmit: 'newMessageForm button[action=submitMessage]',
            sentMessages: 'sentMessages',
            messageDetails: 'messageDetails',
            messageDetailsHeader: 'messageDetails #messageDetailsHeader',
            messageDetailsBody: 'messageDetails #messageDetailsBody',
            messagePictures: 'messageDetails #messagePictures',
            messageDetailsRecipients: 'gridFace#messageDetailsRecipients',
            newMessageChildren: 'gridFace#newMessageChildren',
            newMessageSelectAll: 'messagesCard button[action=selectAll]',
            textField: 'newMessageForm fieldset textfield',
            fileBtn: 'newMessageForm #fileBtn',
            deleteIcon: 'messageDetails #deleteIcon'
        },

        control: {

            fileBtn: {

                success: function (filesArray) {
                    var filesStore = Ext.getStore('Files');
                    filesArray.forEach(function (file) {
                        filesStore.add(file);
                    });

                },
                failure: function () {
                    console.info(arguments);
                }
            },
            deleteIcon: {
                tap: 'deleteIconTapped'
            },

            messagesCard: {
                activate: 'onActivateCard'
            },

            newMessageButton: {
                tap: 'newMessage'
            },

            newMessageContainer: {
                deactivate: 'onDeactivateNewMessageContainer'
            },

            newMessageFormSubmit: {
                tap: 'submitMessage'
            },

            sentMessages: {
                select: 'populateSelectMessage'
            },

            newMessageChildren: {
                itemtap: 'selectChild'
            },

            newMessageSelectAll: {
                tap: 'selectAllChildren'
            },

            textField: {
                change: 'toggleSendButton'
            }
        }
    },

    onActivateCard: function () {
        console.log("Message Tab activated");

        this.loadSentMessages();
    },

    onDeactivateNewMessageContainer: function () {
        //this.getNewMessageButton().show();
        //this.getNewMessageSelectAll().hide();
    },

    loadSentMessages: function () {
        var sentMessages = this.getSentMessages();
        var messagesStore = sentMessages.getStore();
        messagesStore.currentPage = 0;
        messagesStore.load({
            groupInstanceId: SA.app.currentGroupInstanceId,
            callback: function (records, operation, success) {
                console.log('SentMessages loaded', records, operation, success);

                //Set Default Item
                if (records.length > 0) {
                    sentMessages.select(0);
                }
            },
            scope: this
        })
    },

    newMessage: function () {
        //Load Children and go to new Message
        var picturesStore = Ext.getStore('Files')
            , self = this;
        picturesStore.clearData();
        // picturesStore;
        this.newMessageTo = [];
        var newMessageContainer = this.getNewMessageContainer();
        this.loadChildren(function () {

            this.getMessagesCard().push(newMessageContainer);
            self.updateToField();
            //Hide New Button
            //this.getNewMessageButton().hide();

            //Show SelectAll Children button
            //this.getNewMessageSelectAll().show();
        });
    },

    loadChildren: function (callback) {
        var
            that = this,
            childrenStore = Ext.getStore('Children'),
            recipientsStore = Ext.getStore('MessageGroupOutboundRecipients');

        recipientsStore.clearData();

        childrenStore.load({
            groupInstanceId: SA.app.currentGroupInstanceId,
            callback: function (records, operation, success) {
                if (success && records.length > 0) {
                    callback.call(that);
                }
            }
        });
    },
    getUtcDate: function (date) {
        date.setMinutes(date.getMinutes() + date.getTimezoneOffset());

        return date;
    },

    submitMessage: function () {
        var
            sendTime = new Date();

        form = this.getNewMessageForm(),
            fields = form.getFields(),
            message = Ext.create('SA.model.PostMessageGroupOutbound', {
                MessageGroupOutbound: Ext.create('SA.model.MessageGroup', {
                    SendTime: sendTime,
                    Subject: fields.subject.getValue(),
                    Body: fields.message.getValue(),
                    IsMessageWatched: false,
                    GroupInstanceId: SA.app.currentGroupInstanceId

                }).data
            });

        //Add the selected children to the message
        var children = message.ChildIdRecipient();
        var attachedFiles = message.AttachedFiles();

        Ext.Array.forEach(this.newMessageTo, function (o) {
            children.add({Id: o.get('Id')});
        });
        var newMessageAttachedFiles = Ext.getStore('Files').data.items;
        Ext.Array.forEach(newMessageAttachedFiles, function (file) {
            attachedFiles.add({
                ContentType: file.get('ContentType'),
                Location: file.get('Location'),
                Name: file.get('Name')
            });// file.raw);
        });


        //Send the message to server
        message.save({
            success: function (record, operation) {
                console.log('Message Submitted', record, operation);

                //Reload last messages
                this.loadSentMessages();
                this.populateSelectMessage();

                //Remove the newMessage view from NavigationView
                this.getMessagesCard().pop();

                //Reset the form view
                this.resetForm();
            },
            failure: function (record, operation) {
                console.log('this.proxyFailure', arguments);
            }
        }, this);
    },

    populateSelectMessage: function (scope, record, eOpts) {
        var messageDetails = this.getMessageDetails(),
            messageDetailsHeader = this.getMessageDetailsHeader(),
            messageDetailsBody = this.getMessageDetailsBody(),
            messagePictures = this.getMessagePictures(),
            filesStore = Ext.getStore('Files');
        filesStore.clearData();
        this.activeMessage = record;
        //Get the recipients
        if (!record)
        {
            messageDetailsHeader.setDataHeader(null);
            messageDetailsBody.setData(null);
            return;
        }
        this.getMessageDetailsRecipients().getStore().load({
            messageId: record.get('Id'),
            callback: function (records, operation, success) {
                console.log('Selected Message', scope, record, eOpts, records, operation, success);

                if (success) {
                    record.countRecipients = records.length;

                    var attachedFiles = record.raw.AttachedFiles;

                    //Set Data for Message Details Views
                    messageDetailsHeader.setDataHeader(record);
                    messageDetailsBody.setData(record);
                    if (attachedFiles && attachedFiles.length !== 0) {
                        attachedFiles.forEach(function (file) {
                            filesStore.add(file);
                        });
                        messagePictures.show();
                    }
                    else {
                        messagePictures.hide();
                    }
                    //  messagePictures.setData(attachedFiles.data);
                }
            }
        });
    },

    selectChild: function (dataview, index, target, record, e, eOpts) {
        //Change the IsSelected flag
        var isSelected = record.get('IsSelected');
        record.set('IsSelected', !isSelected);

        //Build the selected children array
        this.addRemoveTappedRecord(record, !isSelected);

        //Update To field
        this.updateToField();
    },
    deleteIconTapped: function () {
        Ext.Msg.confirm(Ux.locale.Manager.get('calendar.ConfirmEventDelete'),
            Ux.locale.Manager.get('calendar.confirmRemovingMessage'), function (buttonId) {
                if (buttonId === "yes") {
                    var message = this.activeMessage, self = this;

                    var store = Ext.getStore('Messages');

                    // var originalProxy = store.getProxy();

                    message.setProxy('messageStatusDeleted');

                    console.log(store);

                    //  if (!message.get('isWatched')) {
                    var userStore = Ext.getStore('User');

                    message.save({
                        messageId: message.getId(),

                        success: function (record, operation) {
                            console.log('Message Status Updated', message);
                            message.set('isWatched', true);
                            // message.setProxy('messageUpdateStatus');
                            self.loadSentMessages();
                        },
                        failure: function (record, operation) {
                            console.error('this.proxyFailure', arguments);
                            //message.setProxy('messageUpdateStatus');
                        }
                    });
                    //}

                    console.log('deleteIcon tapped');
                }
            }, this);
    },
    addRemoveTappedRecord: function (record, isSelected) {
        var form = this.getNewMessageForm(),
            fields = form.getFields(),
            recordId = -1,
            recordInArrayIdx = -1;

        //Add record to array or Remove if it already exists
        if (isSelected) {
            this.newMessageTo.push(record);
        } else {
            recordId = record.get('Id');

            this.newMessageTo.filter(function (o, idx) {
                if (o.get('Id') === recordId) {
                    recordInArrayIdx = idx;
                    return true;
                }
            })

            //remove from array
            Ext.Array.splice(this.newMessageTo, recordInArrayIdx, 1);
        }
    },

    updateToField: function () {
        var form = this.getNewMessageForm(),
            fields = form.getFields(),
            to = "";

        //Build the To String
        if (this.newMessageTo.length > 1) {
            to = Ext.String.format("({1} {0}) ",
                Ux.locale.Manager.get('messages.newMessage.selectedRecipients'),
                this.newMessageTo.length);
        }

        Ext.Array.forEach(this.newMessageTo, function (o) {
            to += o.get('PrivateName') + ", ";
        });

        //Remove the last comma
        to = to.substr(0, to.length - 2);

        //Set the To Field with all the names
        fields.to.setValue(to);
    },

    selectAllChildren: function () {
        var that = this, store = Ext.getStore('Children');

        //Reset the children array
        this.newMessageTo = [];

        store.each(function (record, idx, len) {
            record.set('IsSelected', that.isAllSelected);

            that.selectChild(null, idx, this, record);
        });

        this.isAllSelected = !this.isAllSelected;
    },

    resetForm: function () {
        //Reset the form
        var messageForm = this.getNewMessageForm();
        if (messageForm) {
            messageForm.reset();
        }

        //Reset the face children
        this.isAllSelected = false;
        this.selectAllChildren();
    },

    toggleSendButton: function () {
        var form = this.getNewMessageForm(),
            fields = form.getFields();

        if (this.newMessageTo.length > 0 && fields.subject.getValue() && fields.message.getValue()) {
            this.getNewMessageFormSubmit().enable();
        } else {
            this.getNewMessageFormSubmit().disable();
        }
    }
});