Ext.define('SA.controller.Settings', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            settingsMenu: 'settingsMenu',
            main: 'main'
        },

        control: {
            'main': {
                show: 'setGroupInstances',
                onTapSettings: 'openSettings'
            },

            'menu > button': {
                tap: 'doOnMenuClick'
            },

            'settingsMenu': {
                hide: 'onCloseSettings'
            }
        }
    },

    setGroupInstances: function () {
        var store = Ext.getStore('GroupInstance');

        if (store.isLoaded()) {
            this.getSettingsMenu().setData(store.getData());
        }
    },

    openSettings: function () {
        this.activeItem = this.getActiveItem();

        this.toggleMenu();
    },

    onCloseSettings: function () {
        var activeItem = this.getActiveItem();

        this.activeItem = null;

        var mainView = this.getMain();
        mainView._activeItem = null;
        mainView.setActiveItem(activeItem);
    },

    toggleMenu: function () {
        Ext.Viewport.toggleMenu("left");
    },

    clearStoresData: function()
    {
        var storeNames = ['MessageGroupOutbound','Events', 'Events2','EventsConfirmation','Files',
        'MessageGroupOutboundRecipients', 'Parents', 'Providers','ReportingInfo',
        'ReportingOptions','Reports','SummaryReports','TodayEvents','Workers'];
        storeNames.forEach(function(storeName)
        {
            var store =   Ext.getStore(storeName);
            if (store)
            {
                store.clearData();
            }

        });
        //var messages = Ext.getStore('Messages');
        //messages.clearData();

    },

    doOnMenuClick: function (btn) {
        var mainView, firstIcon,
            newGroupInstanceId = btn.groupInstanceId || btn.config.groupInstanceId;

        if (newGroupInstanceId) {
            SA.app.currentGroupInstanceId = newGroupInstanceId;
            this.clearStoresData();

            //Reset the activeItem and show Home
            firstIcon = this.getFirstTabIcon();
            mainView = this.getMain();

            if (this.getActiveItem().iconCls === firstIcon) {
                mainView._activeItem = null;
            } else {
                mainView.setActiveItem(null);
            }
            mainView.setActiveItem(0);
        }

        this.toggleMenu();
    },

    getActiveItem: function () {
        var mainView = this.getMain();

        return mainView.getActiveItem();
    },

    getFirstTabIcon: function () {
        var mainView = this.getMain(),
            firstItem = mainView.getItems().first(),
            icon = firstItem && firstItem.iconCls;

        return icon;
    }
});