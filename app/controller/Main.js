Ext.define('SA.controller.Main', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            mainView: 'main'
        },

        control: {
            mainView: {
                initialize: 'initView',
                activeitemchange: 'initView'
            }
        }
    },

    initView: function () {
        var groupInstance = this.getGroupInstance();

        this.setTitle(groupInstance);
    },

    getGroupInstance: function () {
        var
            groupInstanceStore = Ext.getStore('GroupInstance'),
            groupInstance = groupInstanceStore.getById(SA.app.currentGroupInstanceId);

        return groupInstance;
    },

    setTitle: function (groupInstance) {
        var title = groupInstance && groupInstance.get('Name');

        if (title) {
            this.getMainView().setTitle(title);
        }
    }
});