Ext.define('SA.components.MessageDetailsHeader', {
    extend: 'Ext.Container',
    xtype: 'messageDetailsHeader',

    config: {
        dataHeader: {}
    },

    setDataHeader: function (data) {
        var d;
        if(data) {
            d = Ext.Object.merge(data, {
                headerDate: Ux.locale.Manager.get('messages.details.headerDate'),
                headerTo: Ux.locale.Manager.get('messages.details.headerTo'),
                headerRecipients: Ux.locale.Manager.get('messages.details.headerRecipients')
            });

            this.setData(d);
        }
    }

});