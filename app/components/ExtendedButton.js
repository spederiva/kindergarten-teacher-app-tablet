Ext.define('SA.components.ExtendedButton', {
    extend: 'Ext.Button',
    xtype: 'extendedButton',

    config: {
        action: ''
    }
});