Ext.define('SA.view.common.GridFace', {
	extend: 'Ext.DataView',
	xtype: 'gridFace',

	requires: [
		'Ext.DateExtras'
	],

	config: {
		baseCls: 'facesGrid',
		itemTpl: [
			'<div class="image <tpl if="!IsSelected">unselected</tpl>" style="background-image:url({PathPicture})"></div>',
			'<div class="name">{PrivateName}</div>'
		]
	},

	prepareData: function (data, index, record) {
		Ext.apply(data, {
			shortFamilyName: data.FamilyName ? data.FamilyName.substring(0, 1) : ''
		});

		return data;
	}

});