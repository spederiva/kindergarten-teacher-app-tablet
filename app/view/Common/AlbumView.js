/**
 * Created by ranwahle on 5/3/15.
 */
Ext.define('SA.view.common.AlbumView', {
    extend: 'Ext.DataView',
    xtype: 'albumView',

    requires: [
        'Ext.DateExtras'
    ],

    config: {
        baseCls: 'facesGrid',
        itemTpl: [
            //  '<div class="image" style="background-image:url({link})"></div>',
            '<img src="{link}" class="image"/>'
            //'<div>Image <img src="{link}"/></div>',
            //'<div class="name">{ImageName}</div>',
            //'<div cl '</div>'

        ].join('')
    },

    prepareData: function (data, index, record) {
        console.info(data);
        Ext.apply(data, {
            link: data.Location
            // shortFamilyName: data.FamilyName ? data.FamilyName.substring(0, 1) : ''
        });

        return data;
    }

});