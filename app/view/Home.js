Ext.define('SA.view.Home', {
	extend: 'Ext.Container',
	xtype: 'home',

	config:{
		html: [
			'<div style="text-align: center">',
			'<img src="resources/images/logo.png">',
			'</div>'
		]
	}
});