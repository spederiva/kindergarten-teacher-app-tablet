Ext.define('SA.view.contacts.Providers', {
	extend: 'Ext.List',
	xtype: 'contactsProviders',

	requires: [
		'Ext.DateExtras',
		'SA.model.Worker',
		'SA.store.Providers'
	],

	config: {
		title: "contacts.providersTitle",
		store: 'Providers',
		cls: 'providers',
		variableHeights: true,
		grouped: true,
		itemTpl: [
			//'<div class="avatar" style="background-image: url(data/providers/{Id}.jpg);"></div>',
			'<h2>{PrivateName} {FamilyName}</h2>',
			'<h3>{Cellphone} {TelephoneAtHome}</h3>',
			'<h4>{Email}</h4>'
		],
		listeners: {
			initialize: function () {
				this.setTitle(Ux.locale.Manager.get(this.getTitle()));
			}
		}
	}
});