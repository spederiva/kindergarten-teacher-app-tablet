Ext.define('SA.view.contacts.Main', {
	extend: 'Ext.Container',
	xtype: 'contactsMain',

	requires: [
		'Ext.field.Search'
	],

	config: {
		layout: 'fit',
		cls: 'contactsMain',
		items: [
//			{
//				xtype: 'toolbar',
//				id: 'myToolBar',
//				docked: 'top',
//				locales: {title: 'contacts.title'},
//				items: [
//					{
//						xtype: 'searchfield',
//						id: 'SearchName',
//						locales: {placeHolder: 'contacts.placeHolderSearch'},
//						docked: 'right'
//					}
//				]
//			},
			{
				xtype: 'tabpanel',
				tabBar: {
					docked: 'left'
				},
				items: [
					{
						xtype: 'contactsGroups'
					},
					{
						xtype: 'contactsWorkers'
					},
					{
						xtype: 'contactsProviders'
					}
				]
			}
		]
	}
});