Ext.define('SA.view.contacts.Groups', {
	extend: 'Ext.List',
	xtype: 'contactsGroups',

	requires: [
		'Ext.DateExtras',
		'SA.model.ChildWithContacts',
		'SA.store.ChildrenWithContacts'
	],

	config: {
		title: "contacts.groupTitle",
		store: 'ChildrenWithContacts',
		cls: 'groups',
		variableHeights: true,
		grouped: true,
		disableSelection: true,
//		scrollable: {
//			direction: 'both',
//			directionLock: true
//		},
		itemTpl: [
			'<div class="avatar" style="background-image: url({PathPicture});"></div>',
			'<div class="contactBody">',
				'<h1 class="child">{PrivateName} {FamilyName}</h1>',
				'<tpl for="ContactOfChild">',
					'<tpl if="xindex <= 3">',
						'<div class="contacts">',
							'<div class="avatar" style="background-image: url({PathPicture});"></div>',
							'<h2>{PrivateName}</h2>',
							'<h3>{Cellphone}</h3>',
							'<h3>{TelephoneAtHome}</h3>',
							//'<h4>{Email}</h4>',
						'</div>',
					'</tpl>',
				'</tpl>',
			'</div>'
		],
		listeners: {
			initialize: function () {
				this.setTitle(Ux.locale.Manager.get(this.getTitle()));
			}
		}
	}
});