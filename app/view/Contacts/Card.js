Ext.define('SA.view.contacts.Card', {
	extend: 'Ext.NavigationView',
	xtype: 'contactsCard',

	config: {
		fullscreen: true,
		navigationBar: false,
		items: [
			{
				xtype: 'contactsMain'
			}
		]
	}
});