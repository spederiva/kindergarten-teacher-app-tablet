Ext.define('SA.view.contacts.Workers', {
	extend: 'Ext.List',
	xtype: 'contactsWorkers',

	requires: [
		'Ext.DateExtras',
		'SA.model.Worker',
		'SA.store.Workers'
	],

	config: {
		title: "contacts.teamTitle",
		store: 'Workers',
		cls: 'workers',
		variableHeights: true,
		grouped: true,
		disableSelection: true,
		itemTpl: [
			'<div class="avatar" style="background-image: url({PathPicture});"></div>',
			'<div class="contactBody">',
			'<h1>{PrivateName} {FamilyName}</h1>',
			'<h3>{TelephoneAtHome}</h3>',
			'<h3>{Cellphone}</h3>',
			'<h4>{Email}</h4>',
			'</div>'
		],
		listeners: {
			initialize: function () {
				this.setTitle(Ux.locale.Manager.get(this.getTitle()));
			}
		}
	}
});