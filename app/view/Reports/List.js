Ext.define('SA.view.reports.List', {
    extend: 'Ext.dataview.DataView',
    xtype: 'reportsList',

    requires: [],

    config: {
        cls: 'reports',
        useComponents: true,
        defaultType: 'reportsListItem',

        items: [
            {
                xtype: 'reportsListHeader'
            }
        ]
    }
});