Ext.define('SA.view.reports.ModalReport', {
    extend: 'Ext.Container',
    xtype: 'reportsModalReport',

    config: {
        centered: true,
        modal: true,
        hideOnMaskTap: true,
        cls: 'reportsModalReport',
        layout: 'vbox',
        listeners: {
            show: 'initModalOnOpen',
            hide: 'destroy'
        },

        control: {
            '#options': {
                select: 'updateButtonData'
            },
            '#comment': {
                blur: 'updateComment'
            }
        },

        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                items: [
                    {
                        itemId: 'headerImg',
                        xtype: 'image',
                        cls: 'modalReportHeaderImage',
                        src: ''
                    },
                    {
                        itemId: 'headerChildName',
                        xtype: 'title',
                        tpl: '{PrivateName} {FamilyName}'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        itemId: 'headerReportType',
                        xtype: 'title',
                        title: ''
                    }
                ]
            },
            {
                xtype: 'list',
                flex: 1,
                cls: 'options',
                itemId: 'options',
                activeItem: 1,
                store: {
                    model: 'SA.model.ReportingOptions'
                },
                itemTpl: [
                    '<div class="icon {value}"></div>',
                    '<div class="text">{text}</div>'
                ]
            },
            {
                xtype: 'fieldset',
                cls: 'reportFieldset',
                itemId: 'fieldsetComment',

                items: [
                    {
                        xtype: 'textareafield',
                        itemId: 'comment',
                        locales: {
                            placeHolder: 'reports.freeTextPlaceHolder'
                        }
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },

                items: [
                    {
                        locales: {text: 'reports.modalCloseButton'},
                        ui: 'decline',
                        cls: 'modalReportButton close',
                        itemId: 'closeButton'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        locales: {text: 'reports.modalSaveButton'},
                        ui: 'confirm',
                        cls: 'modalReportButton save',
                        itemId: 'saveButton'
                    }
                ]
            },
            {
                xtype:'list',
                itemId: 'remarkText',
                docked: 'bottom',
                itemTpl: ['<img src="{IconUrl}"/>']
            }
        ]
    },

    initModalOnOpen: function () {
        var options = this.down('#options'), store, record, selectedRowIndex;

        if (this.data.record) {
            store = options.getStore();
            selectedRowIndex = this.getSelectedValue(store);
            record = store.getAt(selectedRowIndex) || {};

            this.addDataForFreeText(record);

            this.updateButtonData(null, record);

            this.selectDefaultRow(options, store, selectedRowIndex);
        } else {
            options.deselectAll();
        }

        this.hideComment();
        this.hideOptionList();
    },

    destroy: function () {
        this.data = undefined;

        this.down('#headerReportType').setTitle(undefined);
        this.down('#options').getStore().setData(undefined);
        this.down('#saveButton').setData(undefined);
        this.down('#headerChildName').setData(undefined);
        this.down('#remarkText').setData(undefined);
        this.down('#headerImg').setData(undefined);
        this.down('#comment').setValue(undefined);
    },

    updateData: function (data) {
        this.data = data;

        this.down('#headerReportType').setTitle(Ux.locale.Manager.get(data.reportingTypes.data.Title));
        this.down('#options').getStore().setData(this.getOptions(data));
        this.down('#saveButton').setData(data);

        this.updateChildData();
    },

    updateRemarkText: function(remarkTextData)
    {
        this.down('#remarkText').setData(remarkTextData);
    },


    updateChildData: function () {
        if (this.data.record) {
            this.down('#headerChildName').setData(this.data.record.data.Child);
            this.down('#headerImg').setSrc(this.data.record.data.Child.PathPicture);
            this.down('#comment').setValue(this.getFreeText(this.data));
        }
    },

    hideComment: function () {
        if (!this.data.record) {
            this.down('#fieldsetComment').hide();
        } else {
            this.down('#fieldsetComment').show();
        }
    },

    hideOptionList: function () {
        var
            options = this.down('#options'),
            isHasData = options.getStore().getData().getCount() > 0,
            comment = this.down('#comment');

        if (isHasData) {
            options.show();
            comment.removeCls('expandedField');

        } else {
            options.hide();
            comment.addCls('expandedField');
        }
    },

    getOptions: function (data) {
        var
            options = Ext.clone(data.reportingTypes.data.Options),
            filteredOptions = options && Ext.Array.filter(options, function (o) {
                o.value = o.value + ' ' + data.reportingTypes.data.Class;
                o.text = Ux.locale.Manager.get(o.text);

                return !Ext.isEmpty(o.text);
            });

        return filteredOptions;
    },

    getFreeText: function (data) {
        var
            freeTextReportingTypeId = data.freeTextReportingTypeId,
            comment = Ext.Array.filter(data.record.get('ReportingInfo'), function (obj) {
                return obj.ReportingType === freeTextReportingTypeId;
            }),
            text = comment.length === 1 ? comment[0] : null;

        return text ? text.FreeText || text.MissingItem : null;
    },

    updateButtonData: function (dataView, record) {
        var data,
            saveButton = this.down('#saveButton');

        if (record) {
            data = saveButton.getData();

            data.selectedReportingTypeId = record.getId();

            if(record.freeTextReportingTypeId) {
                data.freeTextReportingTypeId = record.freeTextReportingTypeId;
            }
        }

        //Add Comments attributes to be send when the button is clicked
        saveButton.setData(data);
    },

    updateComment: function (commentField, record) {
        var
            saveButton = this.down('#saveButton'),
            data = saveButton.getData();

        //Add Comments attributes to be send when the button is clicked
        data.comments = commentField.getValue();

        saveButton.setData(data);
    },

    selectDefaultRow: function (options, store, selectedRowIndex) {
        if (store.getCount() > 0) {
            options.select(selectedRowIndex, false, true);
        }
    },

    getSelectedValue: function (store) {
        var
            data = this.data,
            prop = data.reportingTypes.get('Property'),
            reportingInfoArray = data.record.get('ReportingInfo'),
            reportingInfo = Ext.Array.filter(reportingInfoArray, function (obj) {
                return obj[prop];
            });

        if (Ext.isArray(reportingInfo) && reportingInfo.length === 1) {
            return store.find("id", reportingInfo[0][prop]);
        }

        return 0;
    },

    addDataForFreeText: function (record) {
        var freeTextReportingTypeId = this.data.freeTextReportingTypeId;

        if (!record.getId) {
            record.getId = function () {
                return null;
            }

        }

        record.freeTextReportingTypeId = freeTextReportingTypeId;
    }
});