Ext.define('SA.view.reports.ListItem', {
    extend: 'Ext.dataview.component.DataItem',
    xtype: 'reportsListItem',

    requires: [
        'Ext.Img'
    ],

    config: {
        layout: {
            type: 'hbox'
        },
        cls: 'listItem icons',
        items: [
            {
                xtype: 'image'
            },
            {
                xtype: 'extendedButton',
                action: 'Attendance',
                text: ' ',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                action: 'KidExit',
                text: ' ',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                action: 'Health',
                text: ' ',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                action: 'Eating',
                text: ' ',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                action: 'Sleeping',
                text: ' ',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                action: 'MissingItem',
                text: ' '
            },
            {
                xtype: 'extendedButton',
                action: 'Comment',
                text: ' '
            }
        ],
        listeners: {
            initialize: function () {
                this.addTapHoldEvent();
            }
        }
    },

    addTapHoldEvent: function () {
        this.getItems().each(function (item) {
            if (item.xtype === "extendedButton") {
                item.element.on('taphold', function (event, node, options, eOpts) {
                    this.fireEvent('taphold', this, event, eOpts);
                }, item);
            }
        });
    },

    updateRecord: function (record) {
        if (record) {
            if(record.ReportingTypeStore) {
                this.setReportingTypes(record.ReportingTypeStore);
            }

            this.resetClasses();

            this.updateChildImage(record);

            this.updateButton(record);
        }

        this.callParent(arguments);
    },

    setReportingTypes: function (reportingTypesStore) {
        var self = this;

        reportingTypesStore.each(function (type) {
            var reportingType = type.get('Type');

            self.down('extendedButton[action=' + reportingType + ']').show();
        })
    },

    updateChildImage: function (record) {
        var
            that = this,
            childHasOneInstance = record.ChildHasOneInstance,
            picture = childHasOneInstance && childHasOneInstance.get('PathPicture'),
            privateName = childHasOneInstance && childHasOneInstance.get('PrivateName'),
            image = this.down('image');

        if (image && picture) {
            image.setSrc(picture);
            image.setHtml(privateName || null);
            image.alt = privateName;
        }
    },

    updateButton: function (record) {
        var
            that = this,
            reportingTypesStore = Ext.getStore('ReportingTypes'),
            reportingInfo = record.ReportingInfo && record.ReportingInfo();

        reportingInfo && reportingInfo.each && reportingInfo.each(function (report) {
            var
                action,
                property,
                cls,
                optionsArr,
                options,
                value,
                reportingStatus,
                reportingTypeId = report.get('ReportingType'),
                reportingType = reportingTypesStore.getById(reportingTypeId);

            if (!Ext.isEmpty(reportingType)) {
                action = reportingType.get('Type');
                property = reportingType.get('Property');
                cls = reportingType.get('Class');
                optionsArr = reportingType.get('Options');
                reportingStatus = report.get(property);

                if (Ext.isArray(optionsArr) && +reportingStatus) {
                    options = Ext.Array.filter(optionsArr, function (o) {
                        return o.id === reportingStatus;
                    });

                    if (Ext.isArray(options) && options.length === 1) {
                        value = options[0].value;
                    }
                }

                that.setButtonClass(action, cls, value);
            }
        });
    },

    setButtonClass: function (action, cls, value) {
        var button = this.down("button[action=" + action + "]");

        if (button) {
            button.setCls('');

            button.addCls(cls);

            value && button.addCls(value);
        }
    },

    resetClasses: function () {
        this.getItems().each(function (item) {
            var cls = item.getCls();

            if (cls && item.isXType("extendedButton")) {
                item.removeCls(cls);
            }
        });
    }
});
