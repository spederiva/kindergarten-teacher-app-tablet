Ext.define('SA.view.reports.Actions', {
	extend: 'Ext.Toolbar',
	xtype: 'reportsActions',

	config: {
		layout: {
			pack: 'center'
		},
		hideAnimation: {type: 'slide', direction: 'down'},
		showAnimation: {type: 'slide', direction: 'up'},

		items: [
			{
				locales: {text: 'reports.saveButton'},
				iconCls: 'add',
				ui: 'confirm',
				action: 'Save'
			}
		]
	}
});