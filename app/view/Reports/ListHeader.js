Ext.define('SA.view.reports.ListHeader', {
    extend: 'Ext.Container',
    xtype: 'reportsListHeader',

    config: {
        layout: {
            type: 'hbox'
        },
        scrollDock: 'top',
        docked: 'top',
        cls: 'listHeader icons',

        items: [
            {
                xtype: 'extendedButton',
                action: 'Attendance',
                text: ' ',
                cls: 'attendance',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                action: 'KidExit',
                text: ' ',
                cls: 'kidExit',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                action: 'Health',
                text: ' ',
                cls: 'health',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                action: 'Eating',
                text: ' ',
                cls: 'eating',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                action: 'Sleeping',
                text: ' ',
                cls: 'sleeping',
                hidden: true
            },
            {
                xtype: 'extendedButton',
                text: ' ',
                cls: 'missingItem'
            },
            {
                xtype: 'extendedButton',
                text: ' ',
                cls: 'comment'
            }
        ]
    },

    setReportingTypes: function (reportingTypesStore) {
        var self = this;

        reportingTypesStore.each(function (type) {
            var
                reportingType = type.get('Type'),
                button = self.down('extendedButton[action=' + reportingType + ']');

            if(button) {
                button.show();

                self.getParent().fireEvent( 'hideReportingTypeColumn', reportingType );
            }
        })
    }

});