Ext.define('SA.view.reports.Card', {
    extend: 'Ext.NavigationView',
    xtype: 'reportsCard',

    config: {
        navigationBar: false,
        items: [
            {
                xtype: 'reportsMain'
            }
        ]
    }
});