Ext.define('SA.view.reports.Main', {
    extend: 'Ext.Container',
    xtype: 'reportsMain',

    config: {
        layout: 'vbox',
        cls: 'reportsMain',

        items: [
            {
                layout: 'hbox',
                docked: 'top',
                cls: 'pageHeader',
                items: [
                    {
                        itemId: 'reportPageTitle',
                        xtype: 'label',
                        cls: 'pageTitle',
                        html: 'sdsadas',
                        docked: 'left'
                    },
                    {
                        xtype: 'datepickerfield',
                        name: 'reportingDate',
                        dateFormat: SA.utils.Config.getDateTimeFormat().longDateFormat,
                        value: new Date(),
                        cls: 'datepickerfield'
                    },
                    {
                        xtype: 'button',
                        locales: {text: 'reports.saveButton'},
                        action: 'save',
                        ui: 'confirm',
                        disabled: true
                    }
                ]
            },
            {
                xclass: 'SA.view.reports.List',
                flex: 1
            },
            {
                xtype: 'reportsActions',
                docked: 'bottom',
                hidden: true
            }
        ],

        listeners: {
            updatedata: function (me, newData, eOpts) {
                this.down('#reportPageTitle').setHtml(newData.get('Name'));
            }
        }
    }
});