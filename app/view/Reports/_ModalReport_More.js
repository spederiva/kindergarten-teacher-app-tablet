Ext.define('SA.view.reports.ModalReport_More', {
	extend: 'Ext.Container',
	xtype: 'reportsModalReportMore',

	requires: [
		'Ext.MessageBox'
	],

	config: {
		centered: true,
		width: '70%',
		height: '45%',
		modal: true,
		hideOnMaskTap: true,
		cls: 'reportsModalReport',

		layout: {
			type: 'vbox'
		},

		items: [
			{
				itemId: 'header',
				xtype: 'container',
				fflex: 6,
				height: 100,
				cls: 'modalReportHeader',
				tpl: [
					'<img src="{Child.data.PathPicture}" />',

					'<div class="headerDetails">',
					'<h2>{Child.data.PrivateName} {Child.data.FamilyName}</h2>'

//					'<div class="eventType">',
//					'<h2 class="fa fa-check-circle"></h2>',
//					'<h2>Pee</h2>',
//					'</div>',
//					'</div>'
				]
			},
			{
				xtype: 'formpanel',
				flex: 4,
				layout: 'vbox',
				ccls: 'options',
				items: [
					{
						xtype: 'fieldset',
						items: [
							{
								xtype: 'textareafield',
								placeHolder: 'Message',
								name: 'message'
							}
						]
					},
					{
						xtype: 'button',
						ui: 'confirm',
						cls: 'submitMessageButton',
						text: 'OK',
						action: 'modalReportMore_SubmitMessage',
						itemId: 'submitMessage',
						docked: 'bottom'
					}
				]
			}
		]
	},

	updateData: function (newData) {
		this.down('#header').setData(newData.data);
		this.down('#submitMessage').setData(newData.data);
	}
});