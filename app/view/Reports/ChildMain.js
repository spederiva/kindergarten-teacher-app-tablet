/**
 * Created by ranwahle on 9/30/14.
 */
Ext.define('SA.view.reports.ChildMain', {
    extend: 'Ext.Container',
    xtype: 'reportsChildMain',
    require: [
        'SA.view.reports.ChildDetails'
    ],
    config: {
        layout: 'vbox',
        centered: true,
        modal: true,
        hideOnMaskTap: true,
        cls: 'reportsModalReport',
        control:
        {
            '#closeChildDetailsButton':
            {
                tap: function() {
                    //console.log('tapped');
                    this.hide();
                }
            }
        },
        items: [
//            {
//                layout: 'hbox',
//                docked: 'top',
//                cls: 'pageHeader',
//                items: [
////                    {
////                        itemId: 'childDetailsPageTitle',
////                        xtype: 'label',
////                        cls: 'pageTitle',
////
////                        docked: 'left'
////                    }
//
//                ]
//            },
            {
                xclass: 'SA.view.reports.ChildDetails',
                flex: 1
            },
            {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },

                items: [
                    {
                        locales: {text: 'reports.modalCloseButton'},
                        ui: 'decline',
                        cls: 'modalReportButton close',
                        itemId: 'closeChildDetailsButton'
                    }

                ]
            }

        ]
    }
});