Ext.define('SA.view.reports.ChildDetails',
    {
        extend: 'Ext.DataView',
        xtype: 'reportChildDetails',
        requires: [
            'Ext.DateExtras'
        ],
        config: {
            store: 'Children',
            title: 'פרטים אישיים',
            style: 'direction:rtl;',
            cls: 'details faceList',
            xtype: 'list',
            flext: 1,
            scroll: true,
            itemTpl: [
                '<tpl if="HomeTelephone">',
                '<div>',
                '<span style="color: #adadad; font-size: 13px">מספר טלפון עיקרי</span>',
                '<span style="color: #555; font-size: 20px; margin-right: 40px;"><a href="tel:{HomeTelephone+type=phone}">{HomeTelephone}</a></span>',
                '</div>',
                '</tpl>',
                '<div style="clear: both;">',
                '<div class="avatar" style="background-image: url({PathPicture});"></div>',
                '</div>',
                '<div style="clear:both;">',

                '<div  style="float: right; width: 100px; color: #adadad; font-size: 14px">שם פרטי:</div>',
                '<div style="float: right; width: 60%; color: #555; font-size: 20px;  margin-right: 40px;">{PrivateName}</div>',
                '</div>',
                '<div style="clear:both;">',
                '<div  style="float: right; width: 100px;color: #adadad; font-size: 14px">',
                'שם משפחה',
                '</div>',
                '<div style="float: right; width: 60%;color: #555; font-size: 20px;  margin-right: 40px;">{FamilyName}</div>',
                '</div>',
                '<div style="clear:both;">',
                '<div  style="float: right; width: 100px;color: #adadad; font-size: 14px">ת.לידה:</div>',
                '<div style="float: right; width: 60%;color: #555; font-size: 20px; margin-right: 40px;">',
                '{BirthDate:date("j בF ב-Y")}</div>',
                '</div>',
                '<div style="clear:both;">',
                '<div style="float: right; width: 100px;color: #adadad; font-size: 14px;">ת.זהות:</div>',
                '<div style="float: right; width: 60%;color: #555; font-size: 20px;  margin-right: 40px; ">{CardId}</div>',
                '</div>',
                '<div style="clear:both;">',
                '<div  style="float: right; width: 100px;color: #adadad; font-size: 14px">תחביבים:</div>',
                '<div style="float: right; width: 60%;color: #555; font-size: 20px;  margin-right: 40px; ">{Hobbies}</div>',
                '</div>',
                '<div style="clear:both;">',
                '<div  style="float: right; width: 100px;color: #adadad; font-size: 14px">הרגלים:</div>',
                '<div style="float: right; width: 60%;color: #555; font-size: 20px;  margin-right: 40px;">{Habit}</div>',
                '</div>',
                '<div style="clear:both;">',
                '<div  style="float: right; width: 100px;color: #adadad; font-size: 14px">הערות בריאות:</div>',
                '<div style="float: right; width: 60%color: #555; font-size: 20px;  margin-right: 40px;">{HealthProblem}</div>',
                '</div>'
            ]

        },


        applyData: function (data) {
            this.setRecords(data);
            return Ext.pluck(data || [], 'data');
            config.title += data.PrivateName;
        }
    });