Ext.define('SA.view.ContactUs', {
	extend: 'Ext.form.Panel',
	xtype: 'contactpage',
	id: 'contactForm',

	config: {
		layout: {
			type: 'vbox'
		},
		cls: 'contactUs',

		items: [
			{
				xtype: 'fieldset',
				locales:{title: 'contactUs.title'},
				//instructions: 'Email is optional',

				items: [
					{
						xtype: 'textfield',
						locales:{label: 'contactUs.fieldName'},
						name: 'name'
					},
					{
						xtype: 'emailfield',
						locales:{label: 'contactUs.fieldEmail'},
						name: 'email'
					},
					{
						xtype: 'textareafield',
						locales:{label: 'contactUs.fieldMessage'},
						name: 'message'
					}
				]
			},
			{
				xtype: 'button',
				ui: 'confirm',
				locales:{text: 'contactUs.fieldSend'},
				action: 'submitContact'
			}
		]
	}
});