Ext.define('SA.view.children.ChildDetailsContacts', {
	extend: 'Ext.DataView',
	xtype: 'childDetailsContacts',

	requires: [
		'Ext.DateExtras',
		'SA.model.Parent',
		'SA.store.Parents'
	],

	config: {
		baseCls: 'detailsContacts',
		store: 'Parents',
		itemTpl: [
			'<div class="avatar" style="background-image: url(data/parent/{Id}.jpg);"></div>',
			'<h3>{PrivateName} {FamilyName}</h3>',
			'<h4>{BirthDate:date("F j, Y")}</h4>',
			'<h4>{Email}</h4>'
		].join('')
	}
});
