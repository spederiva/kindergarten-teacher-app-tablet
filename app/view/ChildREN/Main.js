Ext.define('SA.view.children.Main', {
	extend: 'Ext.Container',
	xtype: 'childrenMain',

	config: {
		layout: 'vbox',
		cls: 'childrenMain',

		items: [
			{
				xtype: 'panel',
				docked: 'top',
				height: 20,
				id: 'reportTitle',
				tpl: 'Problem Report for {today:date("l, F j, Y")}'
			},
			{
				xtype: 'gridFace',
				store: 'Children',
				flex: 2.5,
				width: '80%'
			},
			{
				xtype: 'panel',
				cls: 'tapText',
				html: 'Tap on a child face in order to make action'
			},
			{
				xtype: 'todayEvents',
				flex: 1
			},
			{
				xtype: 'childActions',
				docked: 'bottom',
				hidden: true
			}

		]
	},

	initialize: function () {
		Ext.getCmp('reportTitle').setData({
			today: new Date()
		});

	}

});