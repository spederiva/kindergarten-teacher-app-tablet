Ext.define('SA.view.children.Card', {
	extend: 'Ext.NavigationView',
	xtype: 'childrenCard',

	config: {
        autoDestroy: false,
		items: [
			{
				xtype: 'childrenMain'
			}
		]
	}
});