(function () {
	var locals = {
		getFaIcon: function (type) {
			var icon;

			switch (type) {
				case 0:
					icon = 'fa-refresh';
					break;
				case 1:
					icon = 'fa-envelope';
					break;
				case 2:
					icon = 'fa-home';
					break;
				case 3:
					icon = 'fa-star';
					break;
				default :
					icon = 'fa-moon-o';
					break;
			}

			return icon;
		},

		setIcons: function (data, index, record) {
			Ext.apply(data, {
				icon: this.getFaIcon(data.Type)
			});

			return data;
		}
	};

	Ext.define('SA.view.children.ChildDetailsReport', {
		extend: 'Ext.Container',
		xtype: 'childDetailsReport',

		requires: [
			'Ext.DateExtras',

			'SA.model.Report',
			'SA.store.Reports',

			'SA.model.SummaryReport',
			'SA.store.SummaryReports'
		],

		config: {
			cls: 'detailsReports',
			layout: 'hbox',
			items: [
				{
					xtype: 'container',
					layout: 'vbox',
					flex: 1,
					baseCls: 'Report',
					items: [
						{
							xtype: 'title',
							title: 'Week 2 - 2-8/01/2014'
						},
						{
							xtype: 'list',
							store: 'Reports',
							flex: 1,

							itemTpl: [
								'<div class="icon fa {icon}"></div>',
								'<div class="date">{Date:date("m/j/Y")}</div>',
								'<div>{Description}</div>'
							],

							prepareData: function (data, index, record) {
								return locals.setIcons(data, index, record);
							}

						}
					]
				},
				{
					xtype: 'container',
					layout: 'vbox',
					flex: 1,
					baseCls: 'SummaryReport',
					items: [
						{
							xtype: 'title',
							title: 'Report - 30 last days'
						},
						{
							xtype: 'list',
							store: 'SummaryReports',
							flex: 1,

							itemTpl: [
								'<div class="icon fa {icon}"></div>',
								'<div class="count">{count}1</div>'
							],

							prepareData: function (data, index, record) {
								return locals.setIcons(data, index, record);
							}
						}
					]
				}
			]
		}
	});
}());