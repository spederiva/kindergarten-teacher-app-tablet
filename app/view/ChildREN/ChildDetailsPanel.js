Ext.define('SA.view.children.ChildDetailsPanel', {
	extend: 'Ext.TabPanel',
	xtype: 'childDetailsPanel',

	config: {
		fullscreen: true,
		cls: 'childDetails',
		tabBarPosition: 'right',

		items: [
			{
				xtype: 'childDetailsInfo',
				//iconCls: 'info',
				title: 'Info'
			},
			{
				xtype: 'childDetailsContacts',
				//iconCls: 'team',
				title: 'Contacts'
			},
			{
				xtype: 'childDetailsReport',
				//iconCls: 'bookmarks',
				title: 'Reports'
			}
		]
	}
});