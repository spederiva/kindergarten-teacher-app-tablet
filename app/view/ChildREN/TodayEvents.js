Ext.define('SA.view.children.TodayEvents', {
	extend: 'Ext.DataView',
	xtype: 'todayEvents',

	requires: [
		'Ext.DateExtras',
		'SA.model.TodayEvents',
		'SA.store.TodayEvents'
	],

	config: {
		store: 'TodayEvents',
		cls: 'todayEvents-list',
		itemTpl: [
			'<div class="type fa {icon}"></div>',
			'<div class="update">{DateTime:date("H:i")}</div>',
			'<div class="description">{Description}</div>'
		].join('')
	},

	getFaIcon: function (type) {
		var icon;

		switch (type) {
			case 0:
				icon = 'fa-refresh';
				break;
			case 1:
				icon = 'fa-envelope';
				break;
			case 2:
				icon = 'fa-home';
				break;
			case 3:
				icon = 'fa-star';
				break;
			default :
				icon = '';
				break;
		}

		return icon;
	},

	prepareData: function (data, index, record) {
		Ext.apply(data, {
			icon: this.getFaIcon(data.Type)
		});

		return data;
	}
});