Ext.define('SA.view.children.ChildActions', {
	extend: 'Ext.Panel',
	xtype: 'childActions',

	config: {
		id: 'actionButtons',
		cls: 'childActions',

		hideAnimation: {type: 'slide', direction: 'down'},
		showAnimation: {type: 'slide', direction: 'up'},

		items: [
			{
				xtype: 'toolbar',
				items: [
					{
						id: 'btnPoopee',
						text: 'Poo-pee',
						iconCls: 'download',
						scope: this
					},
					{
						id: 'btnFood',
						text: 'Food',
						iconCls: 'refresh',
						scope: this
					},
					{
						id: 'btnSleep',
						text: 'Sleep',
						iconCls: 'organize',
						scope: this
					},
					{
						id: 'btnBehavior',
						text: 'Behavior',
						iconCls: 'more',
						scope: this
					},
					{
						id: 'btnHealth',
						text: 'Health',
						iconCls: 'locate',
						scope: this
					},
					{
						id: 'btnPresence',
						text: 'Presence',
						iconCls: 'add',
						scope: this
					},
					{
						id: 'btnNotes',
						text: 'Notes',
						iconCls: 'add',
						scope: this
					},
					{
						id: 'btnDetails',
						text: 'Details',
						iconCls: 'info',
						scope: this
					}
				]
			}
		]
	}


});