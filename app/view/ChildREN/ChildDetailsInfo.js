(function () {
	var locals = {
		getSection: function (title, fields) {
			var section = [
				'<section>',
				'<span>', title, '</span>',
				'<span>', fields, '</span>',
				'</section>'];

			return section.join('');
		}
	}


	Ext.define('SA.view.children.ChildDetailsInfo', {
		extend: 'Ext.Container',
		xtype: 'childDetailsInfo',

		config: {
			cls: 'detailsInfo',
			layout: 'hbox',
			items: [
				{
					xtype: 'container',
					width: '50%',
					id: 'infoTable',
					tpl: [
						locals.getSection('Id', '{Id}'),
						locals.getSection('Name', '{PrivateName} {FamilyName}'),
						locals.getSection('Birthday', '{BirthDate:date("F j, Y")}'),
						locals.getSection('Address', '{Address.Street.Name} {Address.StreetNumber} {Address.City.Name}'),
						locals.getSection('Habit', '{Habit}'),
						locals.getSection('Hobbies', '{Hobbies}'),
						locals.getSection('Health Problems', '{HealthProblem}'),
						locals.getSection('Parent Divorced?', '{IsParentsDevorsed}'),
						locals.getSection('Year Start', '2014 {GroupInstances[0].YearStart}')
					]
				},
				{
					xtype: 'container',
					cls: 'infoImage',
					id: 'infoImage',
					tpl: '<img src="data/pictures/{Id}.jpg">'
				}
			]
		}
	});
}());