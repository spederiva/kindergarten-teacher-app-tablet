Ext.define('SA.view.Main', {
    extend: 'Ext.TabPanel',
    xtype: 'main',

    config: {
        tabBarPosition: 'top',
        activeItem: Helper.getUrlParams('tab') || 0,
        defaultBackButtonText: 'חזור',

        tabBar: {
            title: '&nbsp;',
            cls: 'mainTabBar',
            items: [
                //{},
                {},
                {},
                {},
                {},
                {docked: 'right'}
            ]
        },

        items: [
//            {
//                xclass: 'SA.view.Home',
//                iconCls: 'home'
//            },

            {
                xclass: 'SA.view.reports.Card',
                iconCls: 'reports'
            },

            {
                xclass: 'SA.view.messages.Card',
                iconCls: 'messages'
            },

            {
                xclass: 'SA.view.calendar.Card',
                iconCls: 'calendar'
            },

            {
                xclass: 'SA.view.contacts.Card',
                iconCls: 'user'
            },
            {
                iconCls: 'fa fa-bars'
            }
        ]
    },

    doTabChange: function (tabBar, newTab) {
        //Prevent bubbling and fire event in order to open settings menu
        if (newTab.getIconCls() === 'fa fa-bars') {
            this.fireEvent('onTapSettings');

            return;
        }

        this.callParent(arguments);
    },

    setTitle: function (title) {
        this.getTabBar().setTitle(title)
    }
});