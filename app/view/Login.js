Ext.define('SA.view.Login', {
	extend: 'Ext.form.Panel',
	xtype: 'login',

	requires: [
		'Ext.form.FieldSet',
		'Ext.field.Password',
		'Ext.field.Email'
	],

	config: {
		id: 'loginForm',
		//fullscreen: true,
		cls: 'login',
		style:null,
		masked: {
			xtype: 'loadmask'
		},
		items: [
//			{
//				xtype: 'toolbar',
//				docked: 'top',
//				title   : '&nbsp;s',
//				locales : {
//					title : 'login.title'
//				}
//			},
			{
				xtype: 'image',
				src: 'resources/images/NazarethTheme/logo.png',
				cls: 'loginLogo'
			},

			{
				xtype: 'fieldset',
				locales : {
					//title: 'login.fieldset.title',
					//instructions: 'login.fieldset.instructions'
				},
				items: [
					{
						xtype: 'emailfield',
						name: 'email',
						locales : {
							placeHolder: 'login.fieldset.email.placeHolder'
						},
						autoCapitalize: true,
						required: true,
						clearIcon: true,
                        listeners: {
                            focus: function (comp, e, eopts) {
								if (!Ext.browser.is.PhoneGap )
									return;

                                var ost =  comp.element.dom.offsetTop;
                                this.getParent().getParent().getScrollable().getScroller().scrollTo(0, ost / 2);
                            }
                        }
					},
					{
						xtype: 'passwordfield',
						name: 'password',
						locales : {
							placeHolder: 'login.fieldset.password.placeHolder'
						},
                        listeners: {
                            focus: function (comp, e, eopts) {
								if (!Ext.browser.is.PhoneGap )
									return;
                                var ost = comp.element.dom.offsetTop;
                                this.getParent().getParent().getScrollable().getScroller().scrollTo(0, ost / 2);
                            }
                        }
					}
				]
			},

			{
				xtype: 'button',
				locales : {
					text: 'login.submitButton'
				},
				id: 'login-button',
				scope: this,
				hasDisabled: false,
				ui: 'loginButton',
				style: 'margin: .5em'
			},
			{
				xtype: 'button',
				locales : {
					text: 'login.forgotPassword'
				},
				id: 'forgotPassword-button',
				scope: this,
				hasDisabled: false,
				ui: 'loginButton',
				style: 'margin: .5em'
			}
		],

        listeners: {
            initialize: function () {
                this.setStyle(Ext.String.format('background-size:{0}px', Ext.getBody().getSize().width));
            }
        }

	}
});