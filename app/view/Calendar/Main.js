Ext.define('SA.view.calendar.Main', {
	extend: 'Ext.Container',
	xtype: 'calendarMain',

	config: {
		layout: 'hbox',
		cls: 'calendarMain',
		title: '&nbsp;',
		locales: {
			title: 'calendar.title'
		},

		items: [
			{
				xclass: 'SA.view.calendar.CalendarView',
				flex: 3
			},
			{
				xtype: 'navigationview',
				flex: 2,
				id: 'eventNavigationView',
				navigationBar: false,
				items: [
					{
						xtype: 'titlebar',
						docked: 'bottom',
						items: [
							{
								align: 'left',
								iconCls: 'add',
								action: 'addEvent',
								locales: {
									text: 'calendar.addEvent'
								}
							},
							{
								align: 'left',
								iconCls: 'delete',
								action: 'removeEvent',
								hidden: true,
								ui: 'decline',
								locales: {
									text: 'calendar.removeEvent'
								}
							},
							{
								align: 'right',
								action: 'back',
								hidden: true,
								ui: 'back',
								locales: {
									text: 'calendar.back'
								}
							}
						]
					},
					{
						xclass: 'SA.view.calendar.EventList'
					}
				]
			}
		]
	}
});