Ext.define('SA.view.calendar.EventForm', {
    extend: 'Ext.form.Panel',
    xtype: 'eventform',

    requires: [
        'Ext.field.DatePicker',
        'Ext.field.Toggle',
        'Ext.ux.field.DateTimePicker'
    ],

    config: {
        layout: {
            type: 'vbox'
        },
        cls: 'eventForm',

        items: [
            {
                xtype: 'title',
                id: 'title2',
                title: '&nbsp;',
                locales: {title: 'calendar.newEvent'}
            },
            {
                xtype: 'fieldset',
                items: [
                    {
                        xtype: 'datetimepickerfield',
                        itemId: 'datePicker',
                        locales: {label: 'calendar.fieldLabelDateTime'},
                        name: 'eventTime',
                        dateTimeFormat: SA.utils.Config.getDateTimeFormat().shortDateTimeFormat,
                        picker: {
                            useTitles: false,
                            yearFrom: new Date().getFullYear(),
                            yearTo: new Date().getFullYear() + 5,
                            minuteInterval: 5,
                            ampm: false,
                            value: new Date()
                        },
                        listeners: {
                            change: function (me, newValue) {
                                var
                                    isFirstEventFired = typeof newValue === 'string',
                                    now = new Date(),
                                    newDate = me.getValue(),
                                    timeDiff = Ext.DateExtras.diff(now, newDate, Ext.Date.MINUTE);

                                this.submitButton = this.submitButton || this.getParent().getParent().down('button[action]');
                                this.errorLabel = this.errorLabel || this.getParent().getParent().down('label#errorMessageDateInThePast');

                                if (isFirstEventFired && this.submitButton && this.errorLabel) {
                                    if (timeDiff < -1) {
                                        this.submitButton.disable();
                                        this.errorLabel.show();
                                    } else {
                                        this.submitButton.enable();
                                        this.errorLabel.hide();
                                    }
                                }
                            }
                        }
                    },
                    {
                        xtype: 'textfield',
                        itemId: 'subject',
                        locales: {label: 'calendar.fieldLabelSubject'},
                        name: 'subject'
                    },
                    {
                        xtype: 'textfield',
                        locales: {label: 'calendar.fieldLabelPlace'},
                        name: 'place'
                    },
                    {
                        xtype: 'togglefield',
                        locales: {label: 'calendar.fieldLabelInviteParents'},
                        name: 'isInvitedParent',
                        value: 1
                    },
                    {
                        xtype: 'textareafield',
                        locales: {label: 'calendar.fieldLabelBody'},
                        name: 'body'
                    }
                ]
            },
            {
                xtype: 'button',
                ui: 'confirm',
                //text: 'Send',
                locales: {
                    text: 'messages.newMessage.submitButton'
                },
                action: 'submitEvent',
                style: {
                    'margin-left': '0.5em',
                    'margin-right': '0.5em'
                }
            },
            {
                xtype: 'label',
                cls: 'errorMessage',
                itemId: 'errorMessageDateInThePast',
                locales: {html: 'calendar.errorMessageDateInThePast'},
                hidden: true
            },
            {
                xtype: 'label',
                cls: 'errorMessage',
                itemId: 'errorMessageSubjectMissing',
                locales: {html: 'calendar.errorMessageSubjectMissing'},
                hidden: true
            }
        ],
        listeners: {
            initialize: 'init'
        }
    },

    init: function () {
        this.down('#subject').on('painted', this.checkFormValidation);
        this.down('#subject').on('change', this.checkFormValidation);
    },

    checkFormValidation: function (me) {
        this.submitButton = this.submitButton || this.getParent().getParent().down('button[action]');
        this.errorLabel = this.errorLabel || this.getParent().getParent().down('label#errorMessageSubjectMissing');

        if (Ext.isEmpty(me.getValue())) {
            this.submitButton.disable();
            this.errorLabel.show();
        } else{
            this.submitButton.enable();
            this.errorLabel.hide();
        }
    },

    setData: function (d) {
        if (d) {
            if (Ext.isDate(d.date)) {
                if (d.date.getHours() === 0 && d.date.getMinutes() === 0) {
                    d.date.setHours(8);
                    d.date.setMinutes(0);
                }

                this.down('#datePicker').setValue(d.date);
            }
        }
    }
});