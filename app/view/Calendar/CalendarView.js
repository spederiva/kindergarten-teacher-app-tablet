Ext.define('SA.view.calendar.CalendarView', {
    extend: 'Ext.ux.TouchCalendarView',
    xtype: 'calendarView',

    config: {
        layout: 'fit',
        viewMode: 'month',
        weekStart: 0,
        //value: new Date(),
        eventStore: '',
        cls: 'touch-calendar-view calendarView',
        masked: false,

        plugins: [Ext.create('Ext.ux.TouchCalendarEvents', {
            startEventField: 'EventTime',
            endEventField: 'EventTime',
            eventBarTpl: [
                '<tpl if="Subject">',
                '{Subject}',
                '<tpl else>',
                '-',
                '</tpl>'
            ],
            eventBarSpacing: 6
        })]
    },

    initialize: function () {
        this.callParent(arguments);

        //Set the Store in order to draw the Events
        this.eventStore = Ext.getStore('Events');
    }

});