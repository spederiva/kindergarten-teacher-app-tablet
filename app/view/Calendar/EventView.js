Ext.define('SA.view.calendar.EventView', {
    extend: 'Ext.Container',
    xtype: 'eventview',

    config: {
        event: null,
        layout: 'vbox',
        baseCls: 'eventView',

        items: [
            {
                xtype: 'title',
                tpl: '{Subject}'
            },
            {
                xtype: 'container',
                id: 'eventViewDetails',
                tpl: [
                    '<h2>{detailsWhen}: {EventTime:date("', SA.utils.Config.getDateTimeFormat().longDateFormat, '")}</h2>',
                    '<h2>{detailsTime}: {EventTime:date("G:i")}</h2>',
                    '<h2>{detailsWhere}: {Place}</h2>',
                    '<h3>{detailsWhat}: {Body}</h3>'
                ]
            },

            {
                xtype: 'eventviewfaces',
                itemId: 'eventViewApproved'
            },

            {
                xtype: 'eventviewfaces',
                itemId: 'eventViewNotApproved'
            },

            {
                xtype: 'eventviewfaces',
                itemId: 'eventViewWaiting'
            }
        ]
    },

    updateEvent: function (event) {
        var eventViewApproved, eventViewNotApproved, eventViewWaiting;

        this.down('title').setData(event);

        this.down('#eventViewDetails').setData(Ext.Object.merge(event, this.getStringLiterals()));

        eventViewApproved = this.down('#eventViewApproved');
        eventViewApproved.setData(event.eventConfirmation.raw.ChildrenApproved.$values);
        eventViewApproved.setTitle(Ux.locale.Manager.get('calendar.approvedTitle'));

        eventViewNotApproved = this.down('#eventViewNotApproved');
        eventViewNotApproved.setData(event.eventConfirmation.raw.ChildrenNotApproved.$values);
        eventViewNotApproved.setTitle(Ux.locale.Manager.get('calendar.notApprovedTitle'));

        eventViewWaiting = this.down('#eventViewWaiting');
        eventViewWaiting.setData(event.eventConfirmation.raw.ChildrenNotAnswerYet.$values);
        eventViewWaiting.setTitle(Ux.locale.Manager.get('calendar.waiting'));
    },

    getStringLiterals: function () {
        return {
            detailsWhen: Ux.locale.Manager.get('calendar.detailsWhen'),
            detailsTime: Ux.locale.Manager.get('calendar.detailsTime'),
            detailsWhere: Ux.locale.Manager.get('calendar.detailsWhere'),
            detailsWhat: Ux.locale.Manager.get('calendar.detailsWhat')
        }
    }
});