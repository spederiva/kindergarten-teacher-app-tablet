Ext.define('SA.view.calendar.EventList', {
    extend: 'Ext.Container',
    xtype: 'eventlist',

    config: {
        layout: 'vbox',
        cls: 'eventList',
        items: [
            {
                xtype: 'title',
                id: 'eventListTitle',
                cls: 'eventListTitle',
                tpl: Ext.String.format('{today:date("{0}")}', SA.utils.Config.getDateTimeFormat().longDateFormat),
                initialize: function () {
                    this.setData({
                        today: new Date()
                    });
                }
            },
            {
                xtype: 'list',
                id: 'eventList',
                flex: 1,
                store: 'Events2',
                cls: 'events',
                variableHeights: true,
                disableSelection: true,
                itemTpl: [
                    '<time>{EventTime:date("' + SA.utils.Config.getDateTimeFormat().shortTimeFormat + '")}</time> <span>{Subject}</span>'
                ]
            }
        ]
    }
});