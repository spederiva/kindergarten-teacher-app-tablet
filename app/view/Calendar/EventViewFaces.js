Ext.define('SA.view.calendar.EventViewFaces', {
    extend: 'Ext.Container',
    xtype: 'eventviewfaces',

    config: {
        _scrollable: false,
        layout: 'vbox',
        flex: 1,

        items: [
            {
                xtype: 'label',
                cls: 'listTitle',
                html: '&nbsp;'
            },
            {
                xtype: 'gridFace',
                itemId: 'faces',
                _scrollable: false,
                flex: 1,
                store: {
                    model: 'SA.model.Child'
                }
            }
        ]
    },

    setData: function (data) {
        this.down('#faces').getStore().setData(data);
    },

    setTitle: function(title){
        this.down('label').setHtml(title);
    }
})