Ext.define('SA.view.calendar.Card', {
	extend: 'Ext.NavigationView',
	xtype: 'calendarCard',

	config: {
        navigationBar: false,
		items: [
			{
				xtype: 'calendarMain'
			}
		]
	}
});