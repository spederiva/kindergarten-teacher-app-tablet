Ext.define('SA.view.settings.Menu', {
    extend: 'Ext.Menu',
    xtype: 'settingsMenu',

    config: {
        width: '40%',
        scrollable: 'vertical',
        cls: 'settingsMenu',

        listeners: {
            updatedata: function (me, newData) {
                var items = [];

                newData.each(function (item) {
                    console.info('group instance id', item.getId());
                    var btn = {
                        text: item.get('Name'),
                        iconCls: 'fa fa-angle-right',
                        menu: "groupInstance_" + item.getId(),
                        groupInstanceId: item.getId()
                    };
                    console.debug(btn);
                    items.push(btn);
                });

                this.setItems(items);
            }
        }
    }
});