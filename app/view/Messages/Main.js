Ext.define('SA.view.messages.Main', {
	extend: 'Ext.Container',
	xtype: 'messagesMain',

    requires:[
        'Ext.Label'
    ],

	config: {
		layout: 'hbox',
		cls: 'messagesMain',
		title: '&nbsp;',

		items: [
			{
				docked: 'top',
				layout: 'hbox',
                cls: 'pageHeader',
				items:[
					{
						xtype: 'label',
                        cls: 'pageTitle',
						label: '',
                        locales:{
                            html: 'messages.sentTitle'
                        },
                        docked: 'left'
					},
					{
						xtype: 'button',
                        docked: 'right',
						locales: {
							text: 'messages.newMessageButton'
						},
						action: 'newMessage',
						ui: 'confirm'
					}					
				]
			},
			
			{
				xclass: 'SA.view.messages.SentMessages'
			},
			{
				flex: 1,
				xclass: 'SA.view.messages.MessageDetails'
			}
		]
	}
});