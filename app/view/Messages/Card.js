Ext.define('SA.view.messages.Card', {
	extend: 'Ext.NavigationView',
	xtype: 'messagesCard',

	config: {
		cls: 'messagesCard',
		locales: {
			defaultBackButtonText: ''
		},
		navigationBar: {
			docked: 'top',
			hidden: true,
			backButton: {
				align: 'right'
			},
			items: [
				{
					xtype: 'button',
					align: 'left',
					locales: {
						text: 'messages.selectAll'
					},
					action: 'selectAll'
				}
			]
		},
		items: [
			{
				xtype: 'messagesMain'
			}
		],

		listeners: {
			initialize: function () {
				this.setDefaultBackButtonText(Ux.locale.Manager.get('buttons.back'));
			}
		}
	}


});