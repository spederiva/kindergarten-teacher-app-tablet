Ext.define('SA.view.messages.MessageDetails', {
    extend: 'Ext.Container',
    xtype: 'messageDetails',

    config: {
        layout: 'vbox',
        cls: 'messageDetails',
        items: [
            {
                docked: 'top',
                xtype: 'messageDetailsHeader',
                itemId: 'messageDetailsHeader',
                tpl: [
                    '<div class="detailsHeader">',
                    //'<h4>Sent By: <strong>N/A</strong></h4>',
                    '<h3><span>{headerDate}:</span>  {data.SendTime:date("' + SA.utils.Config.getDateTimeFormat().longDateTimeFormat + '")}</h3>',
                    '<h3><span>{headerTo}:</span> {countRecipients} {headerRecipients}</h3>',
                    '</div>',
                    '<h1 class="messageDetailsSubject">{data.Subject}</h1>'
                ]

            },
            {
                xtype: 'button',
                text: '<img src="resources/images/delete_icon.png" alt="Delete"/>',
                itemId: 'deleteIcon',
                docked: 'right',
                style: 'background: none; border-style: none;'
                //docked: 'top'
            },
            {
                xtype: 'container',
                itemId: 'messageDetailsBody',
                cls: 'messageDetailsBody pageHeader',
                tpl: '{data.Body}'
            },
            {
                cls: 'pageHeader',
                xtype: 'albumView',
                itemId: 'messagePictures',
                store: 'Files',
                flex: 1

            },
            {
                xtype: 'gridFace',
                itemId: 'messageDetailsRecipients',
                flex: 1,
                store: 'MessageGroupOutboundRecipients',
                cls: 'messageDetailsRecipients'
            }

        ]
    }
});