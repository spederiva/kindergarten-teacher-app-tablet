Ext.define('SA.view.messages.SentMessages', {
    extend: 'Ext.List',
    xtype: 'sentMessages',

    requires: [
        'Ext.DateExtras',
        'SA.store.MessageGroupOutbound',
        'Ext.plugin.ListPaging'
    ],

    config: {
        cls: 'sentMessagesPanel',
        plugins:[
            {
                xclass: 'Ext.plugin.ListPaging',
                // item
                autoPaging: true,

                loadMoreText: '',
                noMoreRecordsText: ''

            }
        ],
        itemTpl: [
            '{Subject}',
            '<date>{SendTime:date("',
            SA.utils.Config.getDateTimeFormat().shortDateTimeFormat,
            '")}</date>'
        ],

        store: 'MessageGroupOutbound'
    }
});