Ext.define('SA.view.messages.newMessageForm', {
	extend: 'Ext.form.Panel',
	xtype: 'newMessageForm',

	requires: [
		'Ext.MessageBox'
	],

	config: {
		layout: 'vbox',
		title: 'New Message',
		cls: 'newMessage',
		items: [
			{
				xtype: 'fieldset',
				locales: {
					title: 'messages.newMessage.title'
				},

				items: [
					{
						xtype: 'textfield',
						name: 'to',
						locales: {
							placeHolder: 'messages.newMessage.to.placeHolder'
						},
						clearIcon: false,
						readOnly: true,
						listeners: {
							//focus: function () {
							//	Ext.Msg.alert('Select Children', 'The children should be selected tapping on the right side!', Ext.emptyFn);
							//}
						}
					},
					{
						xtype: 'textfield',
						name: 'subject',
						locales: {
							placeHolder: 'messages.newMessage.subject.placeHolder'
						}
					},
					{
						xtype: 'textareafield',
						name: 'message',
						locales: {
							placeHolder: 'messages.newMessage.message.placeHolder'
						}
					}


				]
			},
			{
				xclass: 'Ext.Container',
				layout: 'vbox',
				//flex: 1,
				items: [
					//Fileup configuration for "Upload file" mode
					{
						itemId: 'fileBtn',
						xtype: 'fileupload',
						cls: 'submitMessageButton',
						autoUpload: true,
						url: '/SmartafWebApi/File/UploadFileWithContext',
						states:
						{
							'browse':{
								text: '<span class="fa fa-upload"></span>'
							}
						}
					}]
			},

			{
				xtype: 'button',
				ui: 'confirm',
				cls: 'submitMessageButton',
				locales: {
					text: 'messages.newMessage.submitButton'
				},
				action: 'submitMessage',
				disabled: true
			},
			{
				xtype: 'albumView',
				//store: 'Pictures',
				itemId: 'pictures',
				//_scrollable: false,
				flex: 1,
				store: 'Files'
				//store: {
				//    model: 'SA.model.Picture'
				//}
			}

		]


	}

});