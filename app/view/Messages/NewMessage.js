Ext.define('SA.view.messages.NewMessage',{
	extend: 'Ext.Container',
	xtype: 'newMessageContainer',

	config: {
		layout: 'hbox',

		items: [
            {
                xtype: 'gridFace',
                itemId: 'newMessageChildren',
                store: 'Children',
                flex: 1
            },
            {
				xtype: 'newMessageForm',
				flex: 1
			}

		]
	}
});