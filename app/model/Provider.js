Ext.define('SA.model.Provider', {
	extend: 'SA.model.Person',

	require: [
		'SA.model.Person'
	],

	config: {
		fields: [
			{name: 'Password', type: 'string'} ,
			{name: 'Cellphone', type: 'string'}
		],

		validations: [
			{type: 'presence', field: 'PrivateName'},
			{type: 'length', field: 'PrivateName', min: 2}
		],
		proxy: {
			type: 'providers'
		}
	}


});