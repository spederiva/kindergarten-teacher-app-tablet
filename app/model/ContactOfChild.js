Ext.define('SA.model.ContactOfChild', {
	extend: 'SA.model.Person',

	config: {
		idProperty: 'Id',
		fields: [
			{name: 'TelephoneAtHome', type: 'string'},
			{name: 'TelephoneAtWork', type: 'string'},
			{name: 'Cellphone', type: 'string'}

		]
	}

});