Ext.define('SA.model.ChildWithContacts', {
	extend: 'SA.model.Child',

	requires: [
		'SA.model.ContactOfChild'
	],

	config: {
		idProperty: 'Id',

		hasMany: {
			model: 'SA.model.ContactOfChild',
			name: 'ContactOfChild',
			associationKey: 'ContactChildDtoContactDetails'
		},

		proxy: {
			type: 'childrenWithContacts'
		}
	}
});