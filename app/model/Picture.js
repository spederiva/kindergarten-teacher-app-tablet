/**
 * Created by ranwahle on 5/3/15.
 */
Ext.define('SA.model.Picture', {
    extend: 'Ext.data.Model',

    //required: [
    //    'SA.model.Address'
    //],

    config: {
        idProperty: 'Id',
        fields: [
            {name: 'Id', type: 'int'},
            {name: 'link', type: 'string' }

        ]
    }
});
