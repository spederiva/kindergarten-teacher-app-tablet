Ext.define('SA.model.Login', {
	extend: 'Ext.data.Model',

	config: {
		fields: [
			{name: 'email', type: 'string'},
			{name: 'password', type: 'string'},
			{name: 'token', type: 'string'}
		],
		validations: [
			{field: 'email', type: 'presence'},
			{field: 'password', type: 'presence'}
		],
		proxy: {
			type: 'login'
		}
	}


});