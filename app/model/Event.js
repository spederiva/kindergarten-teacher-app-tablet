Ext.define('SA.model.Event', {
	extend: "Ext.data.Model",

	config: {
		//idProperty: 'Id',
		fields: [
			{
				name: 'Id',
				type: 'int'
			},
			{
				name: 'EventTime',
				type: 'date',
				dateFormat: 'c'
			},
			{
				name: 'PublishTime',
				type: 'date',
				dateFormat: 'c'
			},
			{
				name: 'Subject',
				type: 'string'
			},
			{
				name: 'Body',
				type: 'string'
			},
			{
				name: 'Place',
				type: 'string'
			},
			{
				name: 'IsInvitedParent',
				type: 'boolean'
			},
			{
				name: 'GroupInstanceId',
				type: 'int'
			}
		],

		validations: [
			{field: 'Subject', type: 'presence'}
		],

		proxy: {
			type: 'events'
		}
	}
});