Ext.define('SA.model.ReportingInfo', {
	extend: 'Ext.data.Model',

	config: {
		idProperty: 'Id',
		fields: [
			{name: 'Id', type: 'auto'},
			{name: '$type', type: 'string'},
			{name: 'ChildId', type: 'int'},
			{name: 'WorkerId', type: 'int'},
			{name: 'GroupInstanceId', type: 'int'},
			{name: 'ReportingType', type: 'int'},
			{name: 'ReportingDate', type: 'date'},

			{name: 'AttendanceStatus', type: 'int'},
			{name: 'ReportingEatingStatus', type: 'int'},
			{name: 'ReportingStatus', type: 'int'},
            {name: 'ReportingHealthStatus', type: 'int'},
			{name: 'FreeText', type: 'string'},
            {name: 'MissingItem', type: 'string'}
		],
		belongsTo: {
			model: 'SA.model.Report',
			foreignKey: 'reportId'
		}
	}
});