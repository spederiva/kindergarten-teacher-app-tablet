Ext.define('SA.model.SummaryReport', {
	extend: 'Ext.data.Model',

	config: {
		idProperty: 'Id',
		fields: [
			{name: 'Id', type: 'int'},
			{name: 'Type', type: 'int'},
			{name: 'Count', type: 'int'}
		],
		proxy: {
			type: 'summaryReports'
		}
	}
});