Ext.define('SA.model.ReportingOptions', {
	extend: 'Ext.data.Model',

	config:{
		idProperty: 'id',
		fields: [
			{name: 'id', type: 'auto'},
			{name: 'value', type: 'string'},
			{name: 'text', type: 'string'}
		]
	}
});