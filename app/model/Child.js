Ext.define('SA.model.Child', {
    extend: 'SA.model.Person',

    require: [
        'SA.model.Person'
    ],

    config: {
        idProperty: 'Id',
        fields: [
            {name: 'IsParentsDevorsed', type: 'boolean'},
            {name: 'CurrentGroupInstanceId', type: 'int'},
            {name: 'CurrentKindergartenId', type: 'int'},
            {name: 'Habit', type: 'string'},
            {name: 'HealthProblem', type: 'string'},
            {name: 'Hobbies', type: 'string'},
            {name: 'IsSelected', type: 'boolean', defaultValue: false, persist: false}
        ],

//		validations: [
//			{field: 'lastName', type: 'presence'}
//		],

        proxy: {
            type: 'children'
        }
    },

    getFullName: function () {
        return this.get('PrivateName') + ' ' + this.get('FamilyName');
    }

});