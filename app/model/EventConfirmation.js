Ext.define('SA.model.EventConfirmation', {
	extend: 'Ext.data.Model',

	config: {
		fields: [
			{
				name: 'id',
				type: 'auto'
			}
		],

		hasMany: [
			{
				model: 'SA.model.Child',
				name: 'ChildrenNotApproved',
				associationKey: 'ChildrenNotApproved'
			},

			{
				model: 'SA.model.Child',
				name: 'ChildrenNotAnswerYet',
				associationKey: 'ChildrenNotAnswerYet'
			},

			{
				model: 'SA.model.Child',
				name: 'ChildrenApproved',
				associationKey: 'ChildrenApproved'
			}
		],

		proxy: {
			type: 'eventsconfirmation'
		}
	}
})