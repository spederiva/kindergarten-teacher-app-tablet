Ext.define('SA.model.GroupInstance', {
	extend: 'Ext.data.Model',

	config: {
		idProperty: 'Id',
		fields: [
			{name: 'YearStart', type: 'int'},
			{name: 'KinderGratenLevel', type: 'int'},
			{name: 'Name', type: 'string'},
			{name: 'RegistrationStatus', type: 'int'},
			{name: 'IsPaid', type: 'boolean'},
			{name: 'BranchId', type: 'int'},
			{name: 'CurrentCloudConfiguration', type: 'string'},  //CurrentCloudConfiguration
			{name: 'email', type: 'string'}
		]
	}
});