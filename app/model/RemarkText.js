/**
 * Created by ranwahle on 9/26/15.
 */
Ext.define('SA.model.RemarkText', {
    extend: 'Ext.data.Model',


    config: {
        fields: [
            {name: 'IconUrl', type: 'string'} ,
            {name: 'Text', type: 'string'}
        ],
        proxy: null


    }


});