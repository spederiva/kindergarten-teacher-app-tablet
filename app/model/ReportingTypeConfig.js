Ext.define('SA.model.ReportingTypeConfig', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            {name: 'ReportingTypeId', type: 'auto'}
        ],
        proxy: {
            type: 'reportingTypesConfig'
        }
    }
});