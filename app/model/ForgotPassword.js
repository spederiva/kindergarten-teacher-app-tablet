/**
 * Created by ranwahle on 12/22/14.
 */
Ext.define('SA.model.ForgotPassword', {
    extend: 'Ext.data.Model',
    config:
    {
        fields:[
            {name: 'Email', type: 'string'}
        ],
        proxy:
        {
            type: 'forgotPassword'
        }
    }
});