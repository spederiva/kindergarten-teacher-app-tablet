Ext.define('SA.model.Parent', {
    extend: 'Ext.data.Model',

    config: {
        idProperty: 'Id',
        fields: [
            {name: 'Id', type: 'int'},
	        {name: 'PrivateName', type: 'string'},
            {name: 'FamilyName', type: 'string'},
            {name: 'BirthDate', type: 'date'},
	        {name: 'UserName', type: 'string'},
	        {name: 'Email', type: 'string'}
        ],
        proxy:{
            type: 'parents'
        }
    }
});