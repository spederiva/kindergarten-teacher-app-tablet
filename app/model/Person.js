Ext.define('SA.model.Person', {
	extend: 'Ext.data.Model',

	required:[
		'SA.model.Address'
	],

	config: {
		idProperty: 'Id',
		fields: [
			{name: 'Id', type: 'int'},
			{name: 'PrivateName', type: 'string'},
			{name: 'FamilyName', type: 'string'},
			{name: 'CardId', type: 'string'},
			{name: 'BirthDate', type: 'date'},
			{name: 'UserName', type: 'string'},
			{name: 'Email', type: 'string'},
			{name: 'Address', type: 'Address'},
			{name: 'Gender', type: 'int'},
			{name: 'TelephoneAtHome', type: 'string'},
			{name: 'KeyForSearch', convert: function (value, record) {
				var keyForSearch = record.get('PrivateName') + ' ' + record.get('FamilyName');

				return keyForSearch.toLowerCase();
			}},
            {name: 'PathPicture', type: 'string', convert: function (v, record) {
                var picture = record.get('PathPicture') || (record.raw && record.raw.PathPicture);

                if (Ext.isEmpty(picture)) {
                    var
                        isMale = record.get('Gender') !== 2,
                        pic = isMale ? 'boy' : 'girl';

                    return 'resources/images/defaultAvatars/' + pic + '.png';
                }

                return picture;
            }}
		]
	}
});