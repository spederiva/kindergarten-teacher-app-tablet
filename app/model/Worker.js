Ext.define('SA.model.Worker', {
	extend: 'SA.model.Person',

	require:[
		'SA.model.Person'
	],

	config: {
		fields: [
			{name: 'Password', type: 'string'},
			{name: 'TelephoneAtHome', type: 'string'},
			{name: 'TelephoneAtWork', type: 'string'},
			{name: 'Cellphone', type: 'string'}
		],

		validations: [
			//{type: 'presence', field: 'age'},
			//{type: 'length', field: 'name', min: 2}
		],
		proxy:{
			type: 'workers'
		}
	}


});