/**
 * Created by ranwahle on 5/4/15.
 */
/**
 * Created by ranwahle on 5/3/15.
 */
Ext.define('SA.model.File', {
    extend: 'Ext.data.Model',

    //required: [
    //    'SA.model.Address'
    //],

    config: {
        idProperty: 'Id',
        fields: [
            {name: 'ContentType', type: 'string'},
            {name: 'Location', type: 'string' },
            {name: 'Name', type: 'string' },
            {name: 'Size', type: 'int' }

        ]
    }
});
