Ext.define('SA.model.Report', {
	extend: 'Ext.data.Model',

	requires: [
		'SA.model.ReportingInfo'
	],

	config: {
		idProperty: 'Id',
		fields: [
			{name: 'Id', type: 'auto'}
		],

		hasOne: [
			{model: 'SA.model.Child', name: 'Child'},
			{model: 'SA.model.Worker', name: 'Worker'}
		],

		hasMany: [
			{name: 'ReportingInfo', model: 'SA.model.ReportingInfo', associationKey: 'BaseReportingInfoDtos'},
			{name: 'ReportingType', model: 'SA.model.ReportingType'}
		],

		proxy: {
			type: 'reports'
		}
	}
});