Ext.define('SA.model.TodayEvents', {
	extend: 'Ext.data.Model',

	config: {
		idProperty: 'Id',
		fields: [
			{name: 'Id', type: 'int'},
			{name: 'DateTime', type: 'date'},
			{name: 'Type', type: 'int'},
			{name: 'Description', type: 'string'}
		],
		proxy:{
			type: 'todayEvents'
		}
	}

});