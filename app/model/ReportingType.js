Ext.define('SA.model.ReportingType', {
	extend: 'Ext.data.Model',

	config:{
		idProperty: 'Id',
		fields: [
			{name: 'Id', type: 'auto'},
			{name: 'Type', type: 'string'},
			{name: 'Property', type: 'string'},
			{name: 'Title', type: 'string'},
			{name: 'Options', type: 'object'},
			{name: 'Class', type: 'string'},
			{name: '$type', type: 'string'}
		]
	}
});