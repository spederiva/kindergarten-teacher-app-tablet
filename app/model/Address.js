Ext.define('SA.model.Address', {
	extend: 'Ext.data.Model',

	required:[
		'SA.model.Country',
		'SA.model.City'
	],

	config: {
		idProperty: 'Id',
		fields: [
			{name: 'Id', type: 'int'},
			{name: 'Country', type: 'Country'},
			{name: 'City', type: 'City'},
			{name: 'StreetId', type: 'int'},
			{name: 'Street', type: 'Street'}
		]
	}
});