Ext.define('SA.model.MessageGroup', {
    extend: 'Ext.data.Model',
   requires: ['SA.model.File'],
    config: {
        idProperty: 'Id',
        fields: [
            {name: 'Id', type: 'auto'},
            {name: 'SendTime', type: 'datetime'},
            {name: 'Subject', type: 'string'},
            {name: 'Body', type: 'string'},
            {name: 'IsMessageWatched', type: 'boolean'},
            {name: 'GroupInstanceId', type: 'int'}

        ],
        hasMany: {
            name: 'AttachedFiles', model: 'SA.model.File'
        }
    }
})