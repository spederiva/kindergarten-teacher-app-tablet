Ext.define('SA.model.PostMessageGroupOutbound', {
    extend: 'Ext.data.Model',

    requires: [
        'SA.model.MessageGroup',
        'SA.model.ChildRecipient',
        'SA.model.File'
    ],

    config: {
        fields: [
            {name: 'MessageGroupOutbound', type: 'SA.model.MessageGroup'}
        ],

        hasMany: [
            {
                name: 'ChildIdRecipient',
                model: 'SA.model.ChildRecipient',
                associationKey: 'ChildIdRecipient'
            },
            {
                name: 'AttachedFiles',
                model: 'SA.model.File',
                associationKey: 'AttachedFiles'
            }
        ],

        proxy: {
            type: 'messages'
        }
    }
});