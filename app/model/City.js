Ext.define('SA.model.City', {
	extend: 'Ext.data.Model',

	config: {
		idProperty: 'Id',
		fields: [
			{name: 'Id', type: 'int'},
			{name: 'Name', type: 'string'}
		]
	}
});