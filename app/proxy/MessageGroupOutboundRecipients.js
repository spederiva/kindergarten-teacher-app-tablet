Ext.define('SA.Proxy.MessageGroupOutboundRecipients', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.messageGroupOutboundRecipients',
	require: ['SA.utils.Config'],
	config: {
		api: {
			//SA.utils.Config.getBaseUrl()
			read:  SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/Message/{0}/MessageGroupOutboundRecipients'

		}
	},

	buildRequest: function (operation) {
		var
			request = this.callParent(arguments),
			url = Ext.String.format(request.getUrl(), operation.config.messageId);

		request.setUrl(Ext.urlAppend(url));

		return request;
	}
});