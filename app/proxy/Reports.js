Ext.define('SA.Proxy.Reports', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.reports',

	config: {
		api: {
			///SmartAfWebApi/Reporting/ReportingInfoByDateByGroupInstanceId/{groupInstanceId}/{day}/{month}/{year}
			//SA.utils.Config.getBaseUrl()
			read: SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/Reporting/ReportingInfoByDateByGroupInstance/{0}/{1}/{2}/{3}'
		}
	},

	buildRequest: function (operation, callback, scope) {
		var
			request = this.callParent(arguments),
			groupInstanceId = operation.config.data.groupInstanceId,
			day = operation.config.data.day,
			month = operation.config.data.month,
			year = operation.config.data.year,
			url = Ext.String.format(request.getUrl(), groupInstanceId, day, month, year);

		request.setUrl(Ext.urlAppend(url));

		return request;
	}
});