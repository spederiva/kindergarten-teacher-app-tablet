Ext.define('SA.Proxy.Parents', {
    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.parents',
    require: ['SA.utils.Config'],
    config: {
        api: {
            //SA.utils.Config.getBaseUrl() +
	        read: SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/Child/1/Parents'
        },

        reader: {
            rootProperty: 'parents',
            type: 'json'
        }
    }
});