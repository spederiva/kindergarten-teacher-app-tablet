Ext.define('SA.Proxy.Providers', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.providers',
	require: ['SA.utils.Config'],

	config: {
		api: {
			//SA.utils.Config.getBaseUrl()
			read:   SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/KindergartenProviders/{0}'
		},

		read: {
			rootProperty: '',
			type: 'json'
		}
	},

	buildRequest: function (operation) {
		var
			request = this.callParent(arguments),
			url = Ext.String.format(request.getUrl(), operation.config.groupInstanceId);


		request.setUrl(Ext.urlAppend(url));

		return request;
	}
});