Ext.define('SA.Proxy.ReportingTypesConfig', {
    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.reportingTypesConfig',

    requires: [
        'SA.data.reader.ReportingTypesConfig',
        'SA.utils.Config'
    ],

    config: {
        api: {
            //SmartAfWebApi/GroupInstance/ReportingTypes/{groupInstanceId}
            read: SA.utils.Config.getBaseUrl() + '/SmartAfWebApi/GroupInstance/ReportingTypes/{0}'
        },

        reader: {
            type: 'reportingtypesconfig'
        }
    },

    buildRequest: function (operation, callback, scope) {
        var
            request = this.callParent(arguments),
            groupInstanceId = operation.config.groupInstanceId,
            url = Ext.String.format(request.getUrl(), groupInstanceId);

        request.setUrl(Ext.urlAppend(url));

        return request;
    }
});