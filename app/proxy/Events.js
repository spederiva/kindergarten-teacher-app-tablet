Ext.define('SA.Proxy.Events', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.events',

	_url: 'fake.json',

	queries: {
		read: '<tpl>/SmartAfWebApi/Event/{groupInstanceId}/{month}/{year}</tpl>',
		create: '<tpl>/SmartAfWebApi/Event/CreateEvent</tpl>',
		destroy: '<tpl>/SmartAfWebApi/Event/DeleteEvent</tpl>'
	},

	buildRequest: function (operation) {
		var
			request = this.callParent(arguments),
			action = operation.getAction(),
			queryTpl = '',
			url = this.queries[action],
			params = request.getParams(),
			requestData = operation.config.data;

		if (Ext.isEmpty(requestData)) {
			requestData = '';
		}

		//Apply the template in order to add parameters to the URL
		queryTpl = Ext.create('Ext.XTemplate', url);
		url = queryTpl.applyTemplate(requestData);

		//Set the url to request object
		request.setUrl(Ext.urlAppend(url), "");

		return request;
	}



});