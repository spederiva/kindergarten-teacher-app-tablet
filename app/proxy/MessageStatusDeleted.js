/**
 * Created by ranwahle on 6/25/15.
 */
Ext.define('SA.Proxy.MessageStatusDeleted', {
    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.messageStatusDeleted',
    require: ['SA.utils.Config'],
    config: {
        api: {
            //SA.utils.Config.getBaseUrl()
            update: SA.utils.Config.getBaseUrl()  + '/SmartafWebApi/Message/UpdateMessageGroupDeleted/{0}'

        }
    },

    buildRequest: function (operation) {
        var
            request = this.callParent(arguments),
            url = Ext.String.format(request.getUrl(), operation.config.messageId);

        request.setUrl(Ext.urlAppend(url));

        return request;
    }
});