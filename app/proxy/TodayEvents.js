Ext.define('SA.Proxy.TodayEvents', {
    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.todayEvents',

    config: {
        api: {
            //create: 'data/students/create.json',
            read: SA.utils.Config.getBaseUrl()  + 'data/todayEvents/todayEvents.json'
            //update: 'data/students/update',
            //destroy: 'data/students/delete'
        },

        reader: {
            rootProperty: 'todayEvents',
            type: 'json'
        },

        writer: {

        }
    },

    _queries: {
        search: Ext.create('Ext.XTemplate', [
            'use "http://github.com/extjs/YQL-Tables/raw/master/kiva/loanSearch.xml" as loansearch; ',
            'use "http://github.com/extjs/YQL-Tables/raw/master/kiva/loanInfo.xml" as loaninfo; ',
            'select * from loaninfo where ids IN (select id from loansearch where status="fundraising"',
            '<tpl if="sort_by"> AND sort_by="{sort_by}"</tpl>',
            '<tpl if="gender"> AND gender="{gender}"</tpl>',
            '<tpl if="region"> AND region="{region}"</tpl>',
            '<tpl if="sector"> AND sector="{sector}"</tpl>',
            '<tpl if="q"> AND q="{q}"</tpl>)'
            // {compiled: true}
        ]),
        read: {}
    },

    _buildRequest: function (operation) {
        var request = this.callParent(arguments),
            queryTpl = this.queries[operation],
            filters = operation.getFilters() || [],
            params = request.getParams(),
            filterData = {};

        Ext.iterate(filters, function (filter) {
            filterData[filter.getProperty()] = filter.getValue();
        });

        delete params.filters;

        if (!Ext.isEmpty(queryTpl)) {
            Ext.applyIf(params, {
                format: 'json',
                q: queryTpl.applyTemplate(filterData)
            });
        }

        request.setParams(params);
        //request.setUrl(Ext.urlAppend(request.getUrl(), Ext.urlEncode(params)));
        request.setUrl(Ext.urlAppend(request.getUrl()));
        return request;
    }

});