Ext.define('SA.Proxy.Workers', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.workers',
requires:['SA.utils.Config'],
	config: {
		api: {
			//Worker/{pGroupInstanceId:int}/Workers
			//SA.utils.Config.getBaseUrl()
			read:  SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/Worker/{0}/WorkersOfKindergarten'
		},

		read: {
			rootProperty: '',
			type: 'json'
		}
	},

	buildRequest: function (operation) {
		var
			request = this.callParent(arguments),
			url = Ext.String.format(request.getUrl(), operation.config.kindergartenId);


		request.setUrl(Ext.urlAppend(url));

		return request;
	}
});