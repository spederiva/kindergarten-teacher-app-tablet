Ext.define('SA.Proxy.ChildrenWithContacts', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.childrenWithContacts',

	config: {
		api: {
			read:  SA.utils.Config.getBaseUrl()  +'/SmartAfWebApi/Child/ByGroupInstance/{0}/Children/Contacts',

			_create: SA.utils.Config.getBaseUrl()  + 'data/students/create.json',
			_update:  SA.utils.Config.getBaseUrl()  +'data/students/update',
			_destroy: SA.utils.Config.getBaseUrl()  + 'data/students/delete'
		},

		_reader: {
			type: 'json'
		},

		_writer: {
		}
	},

	buildRequest: function (operation) {
		var
			request = this.callParent(arguments),
			url = Ext.String.format(request.getUrl(), operation.config.groupInstanceId);

		request.setUrl(Ext.urlAppend(url));

		return request;
	}

});