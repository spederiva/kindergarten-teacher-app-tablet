Ext.define('SA.Proxy.Login', {
    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.login',

    _url: 'fake.json',
require: ['SA.utils.Config'],
    queries: {
        login: Ext.create('Ext.XTemplate', [
            '<tpl>{baseUrl}</tpl>',
            '/SmartAfWebApi/LoginKindergartenAppNew?',
            '<tpl if="email">userName={email}</tpl>',
            '<tpl if="password">&password={password}</tpl>'
        ])
    },

    buildRequest: function (operation) {
        var request = this.callParent(arguments),
            baseUrl = '',
            queryTpl = this.queries.login,
            filters = operation.getRecords()[0].data,
            params = request.getParams(),
            filterData = {};

        Ext.iterate(filters, function (filter) {
            filterData[filter] = filters[filter];
        });

        if (!Ext.isEmpty(queryTpl)) {
            Ext.applyIf(params, {
                format: 'json',
                q: queryTpl.applyTemplate(filterData)
            });
        }


        request.setUrl(Ext.urlAppend(params.q), "");

        return request;
    }

});