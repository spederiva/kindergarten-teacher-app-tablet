Ext.define('SA.Proxy.Messages', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.messages',
	require: ['SA.utils.Config'],
	config: {
		api: {
			//SA.utils.Config.getBaseUrl()
			read:   SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/Child/{0}/Children',
			create: SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/Message/CreateMessageGroupOutbound',
			update:  SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/Message/CreateMessageGroupOutbound',
			delete:  SA.utils.Config.getBaseUrl()  +	'/SmartafWebApi/Message/UpdateMessageGroupDeleted/{0}'

		},

		reader: {
			rootProperty: 'students',
			type: 'json'
		}
	},

	initialize: function () {
		this.callParent(arguments);

		this.getWriter().getRecordData = this.getRecordData;
	},

	getRecordData: function (record) {
		var childIdRecipient, recipients = [], attachedFiles = [];

		//TODO: convert GroupInstanceId to string in order to be sent between apostrphs
		record.get("MessageGroupOutbound").GroupInstanceId = record.get("MessageGroupOutbound").GroupInstanceId.toString();

		//Avoid sending the 'Id' field
		delete record.data.id;
		delete record.data.MessageGroupOutbound.Id;

		if (Ext.isDefined(record.ChildIdRecipient)) {
			childIdRecipient = record.ChildIdRecipient();

			childIdRecipient.each(function (value) {
				recipients.push(value.get('Id').toString());
			});

			record.set('ChildIdRecipients', recipients);
		}
		if (Ext.isDefined(record.AttachedFiles))
		{
			record.AttachedFiles().each(function(file)
			{
				attachedFiles.push({ContentType: file.get('ContentType'),
				Name: file.get('Name'),
				Location: file.get('Location')});
			});

			record.set('AttachedFiles', attachedFiles);
		}

		return record.data;
	}
});