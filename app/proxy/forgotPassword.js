/**
 * Created by ranwahle on 12/22/14.
 */
/**
 * Created by ranwahle on 12/22/14.
 */

Ext.define('SA.Proxy.ForgotPassword',
    {
        extend: 'Ext.data.proxy.Ajax',
        alias: 'proxy.forgotPassword',
        require: ['SA.utils.Config'],
        config: {
            url: SA.utils.Config.getBaseUrl() + '/SmartAfWebApi/ForgotPasswordWorker/{0}'
        }
        ,
        buildRequest: function (operation) {
            var
                request = this.callParent(arguments),
                records = request.getRecords()[0],
                email = records.get("Email"),

                url = Ext.String.format(request.getUrl(), email);

            request.setUrl(Ext.urlAppend(url));

            return request;
        }
    }
);
