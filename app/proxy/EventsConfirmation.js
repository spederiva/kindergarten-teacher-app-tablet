Ext.define('SA.Proxy.EventsConfirmation', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.eventsconfirmation',

	config: {
		api: {
			read:  SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/Event/{0}/EventConfirmationResult'
		},

		reader: {
			type: 'json'
		}
	},

	buildRequest: function (operation) {
		var
			request = this.callParent(arguments),
			url = Ext.String.format(request.getUrl(), operation.config.eventId);

		//request.setParams(params);
		//request.setUrl(Ext.urlAppend(request.getUrl(), Ext.urlEncode(params)));
		request.setUrl(Ext.urlAppend(url));

		return request;
	}

});