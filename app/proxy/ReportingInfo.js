Ext.define('SA.Proxy.ReportingInfo', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.reportinginfo',

	requires: [
		'SA.data.writer.JsonForReportingInfo',
		'SA.utils.Config'
	],

	config: {
		api: {

			//SA.utils.Config.getBaseUrl()
			create: SA.utils.Config.getBaseUrl()  +'/SmartAfWebApi/Reporting/AddReportingInfoByDateByChildren/{0}/{1}/{2}',
			update: SA.utils.Config.getBaseUrl()  +  '/SmartAfWebApi/Reporting/AddReportingInfoByDateByChildren/{0}/{1}/{2}'
		},

		writer: {
			type: 'jsonforreportinginfo',
			writeAllFields: true,
            allowSingle: false,
			rootProperty: 'BaseReportInfos'
		}
	},

	buildRequest: function (operation, callback, scope) {
		var
			day, month, year, url,
			request = this.callParent(arguments);

		if (!Ext.isEmpty(this.params)) {
			day = this.params.day;
			month = this.params.month;
			year = this.params.year;
			url = Ext.String.format(request.getUrl(), day, month, year);

			if (!Ext.isEmpty(this.params.groupInstanceId)) {
				request.setParams({groupInstanceId: this.params.groupInstanceId});
			}

			request.setUrl(Ext.urlAppend(url));
		}

		this.params = null;

		return request;
	},

	setParams: function (params) {
		this.params = params;
	}

});