Ext.define('SA.Proxy.MessageGroupOutbound', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.messageGroupOutbound',
	require: ['SA.utils.Config'],
		config: {
		api: {
			//Message/{pGroupInstanceId:int}/MessagesOutbountOfGroup
			// SA.utils.Config.getBaseUrl()

			read:  SA.utils.Config.getBaseUrl()  +'/SmartAfWebApi/Message/{0}/MessagesOutbountOfGroup'
		}
	},

	buildRequest: function (operation) {
		var
			request = this.callParent(arguments),
			 groupInstanceId = operation.config.groupInstanceId || SA.app.currentGroupInstanceId
			url = Ext.String.format(request.getUrl(), groupInstanceId);



		request.setUrl(Ext.urlAppend(url));

		return request;
	}
});