Ext.define('SA.Proxy.Children', {
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.children',

	config: {
		api: {
			read:  SA.utils.Config.getBaseUrl()  + '/SmartAfWebApi/Child/ByGroupInstance/{0}/Children'
		},

		reader: {
			rootProperty: 'students',
			type: 'json'
		}
	},

	buildRequest: function (operation) {
	    if(!operation.config || !operation.config.groupInstanceId){
            throw "GroupInstanceId was not passed";
        }

        var
			request = this.callParent(arguments),
			url = Ext.String.format(request.getUrl(), operation.config.groupInstanceId);

		request.setUrl(Ext.urlAppend(url));

		return request;
	}

});