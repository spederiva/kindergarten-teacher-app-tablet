Ext.define('SA.utils.Config', {
    singleton: true,

    config: {
        dateTimeFormat:{
            longDateFormat: 'יום l, j לF, Y',
            shortDateFormat: 'l, F j, Y',
            longDateTimeFormat: 'יום l, F j, Y H:i',
            shortDateTimeFormat: 'יום l d/m/Y H:i',
            shortTimeFormat: 'H:i'
        },

        // Decomment to use the remote server config
        baseUrl: Ext.browser.is.PhoneGap ? 'http://smartaf.azurewebsites.net' : ''


        // Decomment to use the local server config
        //baseUrl:  window.device && window.device.name  ? 'http://smartaf.azurewebsites.net' : ''
    },


    constructor: function () {
        this.initConfig();

        this.callParent(arguments);
    }
});