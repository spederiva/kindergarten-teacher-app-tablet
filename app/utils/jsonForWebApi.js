/*
This Json parser is a copy from Ext.JSON, but will place $type attribute as the first attribute
*/

(function (Ext) {
	Ext.define('SA.utils.JsonForWebApi', {
		singleton: true,

		encodeDate: function (o) {
			return '"' + o.getFullYear() + "-"
				+ pad(o.getMonth() + 1) + "-"
				+ pad(o.getDate()) + "T"
				+ pad(o.getHours()) + ":"
				+ pad(o.getMinutes()) + ":"
				+ pad(o.getSeconds()) + '"';
		},

		encode: function (o) {
			return doEncode(o);
		},

		decode: function (json, safe) {
			try {
				return decode(json);
			} catch (e) {
				if (safe === true) {
					return null;
				}
				Ext.Error.raise({
					sourceClass: "Ext.JSON",
					sourceMethod: "decode",
					msg: "You're trying to decode an invalid JSON String: " + json
				});
			}
		}
	});

	var
		pad = function (n) {
			return n < 10 ? "0" + n : n;
		},
		doDecode = function (json) {
			return eval("(" + json + ')');
		},
		doEncode = function (o) {
			if (!Ext.isDefined(o) || o === null) {
				return "null";
			} else if (Ext.isArray(o)) {
				return encodeArray(o);
			} else if (Ext.isDate(o)) {
				return Ext.JSON.encodeDate(o);
			} else if (Ext.isString(o)) {
				if (Ext.isMSDate(o)) {
					return encodeMSDate(o);
				} else {
					return encodeString(o);
				}
			} else if (typeof o == "number") {
				//don't use isNumber here, since finite checks happen inside isNumber
				return isFinite(o) ? String(o) : "null";
			} else if (Ext.isBoolean(o)) {
				return String(o);
			} else if (Ext.isObject(o)) {
				return encodeObject(o);
			} else if (typeof o === "function") {
				return "null";
			}
			return 'undefined';
		},
		m = {
			"\b": '\\b',
			"\t": '\\t',
			"\n": '\\n',
			"\f": '\\f',
			"\r": '\\r',
			'"': '\\"',
			"\\": '\\\\',
			'\x0b': '\\u000b' //ie doesn't handle \v
		},
		charToReplace = /[\\\"\x00-\x1f\x7f-\uffff]/g,
		encodeString = function (s) {
			return '"' + s.replace(charToReplace, function (a) {
				var c = m[a];
				return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
			}) + '"';
		},
		encodeArray = function (o) {
			var a = ["[", ""],
			// Note empty string in case there are no serializable members.
				len = o.length,
				i;
			for (i = 0; i < len; i += 1) {
				a.push(doEncode(o[i]), ',');
			}
			// Overwrite trailing comma (or empty string)
			a[a.length - 1] = ']';
			return a.join("");
		},
		encodeObject = function (o) {
			var
				$type = '$type',
				a = ["{", ""],
				// Note empty string in case there are no serializable members.
				i;

			if(!Ext.isEmpty(o[$type])){
				a.push("\"", $type, "\":", doEncode(o[$type]), ',');
			}

			for (i in o) {
				if (o.hasOwnProperty(i) && i !== $type) {
					a.push(doEncode(i), ":", doEncode(o[i]), ',');
				}
			}
			// Overwrite trailing comma (or empty string)
			a[a.length - 1] = '}';
			return a.join("");
		},
		encodeMSDate = function (o) {
			return '"' + o + '"';
		};
}(Ext));