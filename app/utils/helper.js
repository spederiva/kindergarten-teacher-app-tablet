Ext.define('SA.utils.Helper', {
	singleton: true,
	alternateClassName: 'Helper',

	getUrlParams: function (param) {
		// separating the GET parameters from the current URL
		var
			getParams = Ext.urlDecode(location.search.toLowerCase().substring(1)),
			value = getParams[param.toLowerCase()];

		return isFinite(value) ? +value : value;
	}
});