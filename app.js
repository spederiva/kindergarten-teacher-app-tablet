Ext.Loader.setConfig({
    disableCaching: false,
    paths: {
        Ux: 'touch/src/ux'
    }
});

Ext.application({
    name: 'SA',

    currentWorker: null,
    currentGroupInstanceId: -1,

    requires: [
        'SA.utils.Config',
        'SA.utils.Helper',
        'SA.utils.JsonForWebApi',
        'SA.utils.SizeMonitor', //TEMP fix, Chrome 43 bug
        'SA.utils.PaintMonitor', //TEMP fix, Chrome 43 bug
        'SA.components.ExtendedButton',
        'SA.components.MessageDetailsHeader',
        'SA.components.Fileup',

        'Ext.dataview.List',
        'Ext.MessageBox',

        'Ext.ux.TouchCalendarEvents',
        'Ext.ux.TouchCalendarView',
        'Ext.ux.TouchCalendarMonthEvents',

        'Ux.locale.Manager',
        'Ux.locale.override.st.Component',
        'Ux.locale.override.st.Button',
        'Ux.locale.override.st.Container',
        'Ux.locale.override.st.TitleBar',
        'Ux.locale.override.st.Toolbar',
        'Ux.locale.override.st.Title',
        'Ux.locale.override.st.field.Field',
        'Ux.locale.override.st.field.DatePicker',
        'Ux.locale.override.st.form.FieldSet',
        'Ux.locale.override.st.picker.Picker',
        'Ux.locale.override.st.picker.Date',

        'SA.Proxy.Children',
        'SA.Proxy.Login',
        'SA.Proxy.ForgotPassword',
        'SA.Proxy.Parents',
        'SA.Proxy.Reports',
        'SA.Proxy.ReportingInfo',
        'SA.Proxy.Workers',
        'SA.Proxy.Providers',
        'SA.Proxy.ChildrenWithContacts',
        'SA.Proxy.Events',
        'SA.Proxy.EventsConfirmation',
        'SA.Proxy.Messages',
        'SA.Proxy.MessageGroupOutbound',
        'SA.Proxy.MessageGroupOutboundRecipients',
        'SA.Proxy.MessageStatusDeleted',
        'SA.Proxy.ReportingTypesConfig'
    ],

    stores: [
        'Children',
        'Parents',
        'GroupInstance',
        'Workers',
        'Providers',
        'Files',
        'ChildrenWithContacts',
        'Events',
        'Events2',
        'EventsConfirmation',
        'MessageGroupOutbound',
        'MessageGroupOutboundRecipients',
        'ReportingInfo',
        'Reports',
        'ReportingTypes',
        'ReportingTypesConfig',
        'ReportingOptions',
        'RemarkText'
    ],

    controllers: [
        'Login',
        'NavigationBar',
        'Main',
        'Contacts',
        'Calendar',
        'Messages',
        'Reports',
        'Settings'
    ],

    views: [
        'Main',
        'Login',
        'ContactUs',
        'Home',

        'common.GridFace',
        'common.AlbumView',
        'settings.Menu',

        'contacts.Card',
        'contacts.Main',
        'contacts.Workers',
        'contacts.Providers',
        'contacts.Groups',

        'calendar.Card',
        'calendar.Main',
        'calendar.CalendarView',
        'calendar.EventList',
        'calendar.EventForm',
        'calendar.EventView',
        'calendar.EventViewFaces',

        'messages.Card',
        'messages.Main',
        'messages.NewMessage',
        'messages.newMessageForm',
        'messages.SentMessages',
        'messages.MessageDetails',

        'reports.Card',
        'reports.Main',
        'reports.List',
        'reports.ListItem',
        'reports.ListHeader',
        'reports.Actions',
        'reports.ModalReport',
        'reports.ChildMain',
        'reports.ChildDetails'
    ],

    isIconPrecomposed: true,

    launch: function () {
        console.log('App Launched');

        //Workaround in order to avoid the msg.alert problem!
        Ext.Msg.defaultAllowedConfig.showAnimation = false;
        Ext.Msg.defaultAllowedConfig.hideAnimation = false;

        this.initLocaleManager('he');

        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize the main view
        Ext.Viewport.add({xtype: 'login'});

        //Set the SideMenu - Settings
        var menu = Ext.create('SA.view.settings.Menu');
        Ext.Viewport.setMenu(menu, {
            side: 'left',
            reveal: true
        });
    },

    onReady: function () {
        SA.app.loadDateLocales('he');
    },

    initLocaleManager: function (languageCode) {
        Ux.locale.Manager.setConfig({
            ajaxConfig: {
                method: 'GET'
            },
            language: languageCode || (navigator.language ? navigator.language.split('-')[0] : navigator.userLanguage.split('-')[0]),
            tpl: 'locales/{locale}.json',
            type: 'ajax'
        });

        Ux.locale.Manager.init();
    },

    loadDateLocales: function (languageCode) {
        if (languageCode) {
            var url = Ext.util.Format.format("touch/src/locale/ext-lang-{0}.js", languageCode);

            Ext.Loader.loadScriptFile(
                url,
                function () {
                    console.log(languageCode + ' locales were loaded!');
                },
                function () {
                    console.log('Failed to load locale file.');
                }
            );
        }
    }
});
